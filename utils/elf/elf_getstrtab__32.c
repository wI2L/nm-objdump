/*
** elf_getstrtab__32.c for nm-objdump in /home/poussi_w//projects/nm-objdump/utils/elf
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb 22 20:22:56 2012 william poussier
** Last update Wed Feb 22 20:27:07 2012 william poussier
*/

#include "utils.h"

char		*elf_getstrtab__32(void *buf)
{
  Elf32_Shdr	*scn;
  char		*strtab;

  scn = elf_getscn_by_type__32(buf, SHT_STRTAB);

  if (scn != NULL)
    {
      strtab = buf + scn->sh_offset;
      return (strtab);
    }
  return (NULL);
}
