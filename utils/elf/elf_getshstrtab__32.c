/*
** elf_getshtrtab__32.c for nm-objdump in /home/poussi_w//projects/nm-objdump/utils/elf
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Mon Feb 20 17:18:56 2012 william poussier
** Last update Mon Feb 20 17:27:14 2012 william poussier
*/

#include <elf.h>
#include "utils.h"

char		*elf_getshstrtab__32(void *buf)
{
  Elf32_Ehdr	*elf;
  Elf32_Shdr	*scn;
  char		*shstrtab;

  elf = buf;
  scn = buf + elf->e_shoff + (elf->e_shentsize * elf->e_shstrndx);

  shstrtab = buf + scn->sh_offset;

  return (shstrtab);
}
