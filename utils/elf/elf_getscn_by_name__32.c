/*
** elf_getscn_by_name__32.c for nm-objdump in /home/poussi_w//projects/nm-objdump/utils/elf
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Mon Feb 20 15:51:06 2012 william poussier
** Last update Mon Feb 20 17:22:17 2012 william poussier
*/

#include <elf.h>
#include <string.h>
#include <stdlib.h>
#include "utils.h"

void		*elf_getscn_by_name__32(void *buf, char *name)
{
  Elf32_Ehdr	*elf;
  Elf32_Shdr	*scn;
  char		*shstrtab;
  int		i;

  i = 0;
  elf = buf;

  scn = buf + elf->e_shoff + (elf->e_shentsize * elf->e_shstrndx);
  shstrtab = buf + scn->sh_offset;

  while (i < elf->e_shnum)
    {
      scn = buf + elf->e_shoff + (elf->e_shentsize * i);

      if (strcmp((scn->sh_name + shstrtab), name) == 0)
	return (scn);
      i++;
    }
  return (NULL);
}
