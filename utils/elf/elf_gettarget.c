/*
** elf_gettarget.c for nm-objdump in /home/poussi_w//projects/nm-objdump/elf
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Sun Dec 25 07:09:48 2011 william poussier
** Last update Thu Mar  1 18:26:32 2012 william poussier
*/

#include <elf.h>
#include <stdlib.h>
#include "utils.h"

char	*elf_gettarget(int target)
{
  if (target == EM_386)
    return ("i386");
  if (target == EM_860)
    return ("i860");
  if (target == EM_960)
    return ("i960");
  if (target == EM_IA_64)
    return ("ia-64");
  if (target == EM_X86_64)
    return ("i386:x86-64");

  return ("<unknown target>");
}
