/*
** elf_getarchi.c for nm-objdump in /home/poussi_w//projects/nm-objdump/elf
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Sun Dec 25 07:11:44 2011 william poussier
** Last update Thu Mar  1 18:25:41 2012 william poussier
*/

#include <elf.h>
#include <stdlib.h>
#include "utils.h"

char	*elf_getarchi(int archi)
{
  if (archi == EM_386)
    return ("i386");
  if (archi == EM_860)
    return ("i860");
  if (archi == EM_960)
    return ("i960");
  if (archi == EM_IA_64)
    return ("ia-64");
  if (archi == EM_X86_64)
    return ("x86-64");

  return ("<unknown arch>");
}
