/*
** elf_getscn_by_name__64.c for nm-objdump in /home/poussi_w//projects/nm-objdump/utils/elf
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Tue Feb 21 09:16:05 2012 william poussier
** Last update Tue Feb 21 09:16:27 2012 william poussier
*/

#include <elf.h>
#include <string.h>
#include <stdlib.h>
#include "utils.h"

void		*elf_getscn_by_name__64(void *buf, char *name)
{
  Elf64_Ehdr	*elf;
  Elf64_Shdr	*scn;
  char		*shstrtab;
  int		i;

  i = 0;
  elf = buf;

  scn = buf + elf->e_shoff + (elf->e_shentsize * elf->e_shstrndx);
  shstrtab = buf + scn->sh_offset;

  while (i < elf->e_shnum)
    {
      scn = buf + elf->e_shoff + (elf->e_shentsize * i);

      if (strcmp((scn->sh_name + shstrtab), name) == 0)
	return (scn);
      i++;
    }
  return (NULL);
}
