/*
** elf_check.c for my_objdump in /home/poussi_w//projects/nm-objdump/elf
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Fri Dec 23 09:15:49 2011 william poussier
** Last update Thu Dec 29 00:43:36 2011 william poussier
*/

#include <elf.h>
#include "my_objdump.h"
#include "my_nm.h"

int	elf_check(Elf_Ehdr *elf)
{
  if (elf->e_ident[EI_MAG0] != 0x7f ||
      elf->e_ident[EI_MAG1] != 'E' ||
      elf->e_ident[EI_MAG2] != 'L' ||
      elf->e_ident[EI_MAG3] != 'F')
    {
      return (0);
    }
  return (1);
}
