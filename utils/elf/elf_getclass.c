/*
** elf_getclass.c for nm-objdump in /home/poussi_w//projects/nm-objdump/elf
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Sun Dec 25 07:11:13 2011 william poussier
** Last update Thu Mar  1 18:26:06 2012 william poussier
*/

#include <elf.h>
#include <stdlib.h>
#include "utils.h"

char	*elf_getclass(int class)
{
  if (class == ELFCLASSNONE)
    return ("none");
  if (class == ELFCLASS32)
    return ("elf32");
  if (class == ELFCLASS64)
    return ("elf64");

  return ("<unknown class>");
}
