/*
** elf_getscn_by_type__32.c for nm-objdump in /home/poussi_w//projects/nm-objdump/utils/elf
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Mon Feb 20 15:45:31 2012 william poussier
** Last update Wed Feb 22 21:29:31 2012 william poussier
*/

#include <elf.h>
#include <stdlib.h>
#include "utils.h"

void		*elf_getscn_by_type__32(void *buf, unsigned int type)
{
  Elf32_Ehdr    *elf;
  Elf32_Shdr    *scn;
  int           i;

  i = 0;
  elf = buf;

  while (i < elf->e_shnum)
    {
      scn = buf + elf->e_shoff + (elf->e_shentsize * i);
      if (scn->sh_type == type)
	return (scn);
      i++;
    }
  return (NULL);
}

