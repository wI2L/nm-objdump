/*
** elf_getshstrtab__64.c for nm-objdump in /home/poussi_w//projects/nm-objdump/utils/elf
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Tue Feb 21 09:17:01 2012 william poussier
** Last update Tue Feb 21 09:17:15 2012 william poussier
*/

#include <elf.h>
#include "utils.h"

char		*elf_getshstrtab__64(void *buf)
{
  Elf64_Ehdr	*elf;
  Elf64_Shdr	*scn;
  char		*shstrtab;

  elf = buf;
  scn = buf + elf->e_shoff + (elf->e_shentsize * elf->e_shstrndx);

  shstrtab = buf + scn->sh_offset;

  return (shstrtab);
}
