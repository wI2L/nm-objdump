/*
** elf_getstrtab__64.c for nm-objdump in /home/poussi_w//projects/nm-objdump/utils/elf
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 23 00:23:36 2012 william poussier
** Last update Thu Feb 23 00:23:47 2012 william poussier
*/

#include "utils.h"

char		*elf_getstrtab__64(void *buf)
{
  Elf64_Shdr	*scn;
  char		*strtab;

  scn = elf_getscn_by_type__64(buf, SHT_STRTAB);

  if (scn != NULL)
    {
      strtab = buf + scn->sh_offset;
      return (strtab);
    }
  return (NULL);
}
