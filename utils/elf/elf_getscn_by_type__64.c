/*
** elf_getscn_by_type__64.c for nm-objdump in /home/poussi_w//projects/nm-objdump/utils/elf
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Tue Feb 21 09:15:09 2012 william poussier
** Last update Tue Feb 21 09:15:35 2012 william poussier
*/

#include <elf.h>
#include "utils.h"

void		*elf_getscn_by_type__64(void *buf, unsigned int type)
{
  Elf64_Ehdr	*elf;
  Elf64_Shdr	*scn;
  int		i;

  i = 0;
  elf = buf;

  while (i < elf->e_shnum)
    {
      scn = buf + elf->e_shoff + (elf->e_shentsize * i);
      if (scn->sh_type == type)
	return (scn);
      i++;
    }
  return (0);
}

