/*
** ar_getmember_wh.c for nm-objdump in /home/poussi_w//projects/nm-objdump/utils/ar
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Sun Jan 29 01:47:13 2012 william poussier
** Last update Sun Jan 29 01:50:02 2012 william poussier
*/

#include <ar.h>
#include <stdlib.h>
#include "my_objdump.h"
#include "my_nm.h"

void			*ar_getmember_wh(char *file, void *buf, size_t fsize,
					 t_v_ar *h_ar, int reset)
{
  static unsigned int	offset = SARMAG;
  static unsigned int	old_size = 0;
  void			*data;

  data = NULL;

  if (reset == 1)
    {
      offset = SARMAG;
      old_size = 0;
      return (NULL);
    }

  if (offset >= (fsize - 1))
    return (NULL);

  if (fsize != 0 && offset < fsize)
    data = ar_getmember_data_wh(file, buf, &offset, h_ar, &old_size);

  if (data == NULL)
    return ((void *) AR_NODATA);

  return (data);
}
