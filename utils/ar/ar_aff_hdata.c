/*
** ar_aff_hdata.c for nm-objdump in /home/poussi_w//projects/nm-objdump/utils/ar
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Sun Jan 29 01:29:36 2012 william poussier
** Last update Sun Jan 29 17:46:59 2012 william poussier
*/

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "utils.h"
#include "xlib.h"

void			ar_aff_hdata(t_v_ar *h_ar)
{
  char			mode[AR_MODE];
  char			date[AR_DATE];
  const struct tm	*tm;

  if (h_ar != NULL)
    {
      tm = localtime(&h_ar->date);
      my_strmode(h_ar->mode, mode);

      xstrftime(date, 64, AR_DATE_FORMAT, tm);

      printf("%s %d/%d %6ld %s %s\n",
	     mode,
	     h_ar->uid, h_ar->gid,
	     h_ar->size,
	     date,
	     h_ar->name);
    }
}
