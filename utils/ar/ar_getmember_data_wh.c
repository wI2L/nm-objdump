/*
** ar_getmember_data_wh.c for nm-objdump in /home/poussi_w//projects/nm-objdump/utils/ar
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Sun Jan 29 01:47:36 2012 william poussier
** Last update Sun Jan 29 01:47:45 2012 william poussier
*/

#include <ar.h>
#include <stdlib.h>
#include <string.h>
#include "my_objdump.h"
#include "my_nm.h"

void		*ar_getmember_data_wh(char *file, void *buf, unsigned int *offset,
				      t_v_ar *h_ar, unsigned int *old_size)
{
  t_ar		*ar;
  void		*data;
  char		ar_name[AR_NAME];

  data = NULL;

  if (*old_size & 1)
    *offset += 1;

  ar = (void *)((char *)buf + *offset);
  ar_check_member(ar, file);

  ar_extract_hdata(ar, h_ar);

  strncpy(ar_name, ar->ar_name, AR_NAME);
  ar_name[AR_NAME - 1] = '\0';

  data = ar_parse_data(buf, offset, ar_name, h_ar->name);
  *offset += h_ar->size + sizeof(struct ar_hdr);

  *old_size = h_ar->size;

  return (data);
}
