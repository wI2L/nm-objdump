/*
** ar_getmember.c for nm-objdump in /home/poussi_w//projects/nm-objdump/utils
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Dec 28 23:41:09 2011 william poussier
** Last update Wed Jan 18 11:05:56 2012 william poussier
*/

#include <ar.h>
#include <stdlib.h>
#include "my_objdump.h"
#include "my_nm.h"

void			*ar_getmember(char *file, void *buf, size_t fsize,
				      char *name, int reset)
{
  static unsigned int	offset = SARMAG;
  static unsigned int	old_size = 0;
  void			*data;

  data = NULL;

  if (reset == 1)
    {
      offset = SARMAG;
      old_size = 0;
      return (NULL);
    }

  if (offset >= (fsize - 1))
    return (NULL);

  if (fsize != 0 && offset < fsize)
    data = ar_getmember_data(file, buf, &offset, name, &old_size);

  if (data == NULL)
    return ((void *) AR_NODATA);

  return (data);
}
