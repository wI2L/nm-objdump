/*
** ar_parse_data.c for nm-objdump in /home/poussi_w//projects/nm-objdump/utils
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Dec 29 03:27:30 2011 william poussier
** Last update Sat Dec 31 00:48:53 2011 william poussier
*/

#include <stdlib.h>
#include <string.h>
#include "my_nm.h"
#include "my_objdump.h"

void		*ar_parse_data(void *buf, unsigned int *offset,
			       char *ar_name, char *name)
{
  void		*data;
  int		str_offset;
  static char	*strtab = NULL;

  data = NULL;

  if (strncmp(ar_name, AR_STRTAB, 2) == 0)
    {
      strtab = (void *)((char *)buf + *offset + sizeof(struct ar_hdr));
      return (NULL);
    }

  if (ar_name[0] != AR_END_CHAR)
    {
      data = (void *)((char *)buf + *offset + sizeof(struct ar_hdr));
      name = ar_getmember_name(ar_name, name);
    }
  else if (ar_name[1] >= '0' && ar_name[1] <= '9')
    {
      data = (void *)((char *)buf + *offset + sizeof(struct ar_hdr));
      str_offset = atoi(ar_name + 1);
      name = ar_getmember_name((strtab + str_offset), name);
    }

  return (data);
}
