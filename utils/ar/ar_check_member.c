/*
** ar_check_member.c for nm-objdump in /home/poussi_w//projects/nm-objdump/utils/ar
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Fri Jan 13 13:04:30 2012 william poussier
** Last update Wed Jan 18 11:10:49 2012 william poussier
*/

#include <ar.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "my_nm.h"
#include "my_objdump.h"

void	ar_check_member(t_ar *ar, char *file)
{
  void	*end;

  end = (char *)ar + sizeof(t_ar) - strlen(ARFMAG);

  if (strncmp(end, ARFMAG, 2) != 0)
    {
      fprintf(stderr, AR_MALFORMED, get_progname(NULL), file);
      exit(EXIT_FAILURE);
    }
}
