/*
** ar_check.c for nm-objdump in /home/poussi_w//projects/nm-objdump/utils
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Dec 28 23:02:27 2011 william poussier
** Last update Thu Dec 29 10:28:07 2011 william poussier
*/

#include <ar.h>
#include <string.h>
#include "my_objdump.h"
#include "my_nm.h"

int	ar_check(void *buf)
{
  if (strncmp(buf, ARMAG, SARMAG) == 0)
    {
      return (1);
    }
  return (0);
}
