/*
** ar_getmemberdata.c for nm-objdump in /home/poussi_w//projects/nm-objdump/utils
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Dec 29 03:25:32 2011 william poussier
** Last update Wed Jan 18 11:10:10 2012 william poussier
*/

#include <ar.h>
#include <stdlib.h>
#include <string.h>
#include "my_objdump.h"
#include "my_nm.h"

void		*ar_getmember_data(char *file, void *buf, unsigned int *offset,
				   char *name, unsigned int *old_size)
{
  t_ar		*ar;
  void		*data;
  char		ar_name[AR_NAME];
  unsigned long	ar_size;

  data = NULL;

  if (*old_size & 1)
    *offset += 1;

  ar = (void *)((char *)buf + *offset);
  ar_check_member(ar, file);

  strncpy(ar_name, ar->ar_name, AR_NAME);

  ar_name[AR_NAME - 1] = '\0';
  ar_size = strtoul(ar->ar_size, NULL, 10);

  data = ar_parse_data(buf, offset, ar_name, name);
  *offset += ar_size + sizeof(struct ar_hdr);

  *old_size = ar_size;

  return (data);
}
