/*
** ar_extract_hdata.c for nm-objdump in /home/poussi_w//projects/nm-objdump/utils/ar
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Sat Jan 28 23:01:37 2012 william poussier
** Last update Sun Jan 29 01:17:15 2012 william poussier
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "utils.h"

void	ar_extract_hdata(t_ar *header, t_v_ar *my_ar)
{
  sscanf(header->ar_uid, "%6d", &my_ar->uid);
  sscanf(header->ar_gid, "%6d", &my_ar->gid);
  sscanf(header->ar_mode, "%8o", &my_ar->mode);

  my_ar->size = strtoul(header->ar_size, NULL, 10);
  my_ar->date = strtoul(header->ar_date, NULL, 10);
}
