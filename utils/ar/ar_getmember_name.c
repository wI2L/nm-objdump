/*
** ar_getmembername.c for nm-objdump in /home/poussi_w//projects/nm-objdump/utils
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Dec 29 02:53:35 2011 william poussier
** Last update Sat Dec 31 00:52:02 2011 william poussier
*/

#include <stdlib.h>
#include <string.h>
#include "my_nm.h"
#include "my_objdump.h"

char	*ar_getmember_name(char *buf, char *name)
{
  int	i;

  i = 0;

  if (buf != NULL)
    {
      while (buf[i] != AR_END_CHAR && buf[i] != '\0')
	i++;

      strncpy(name, buf, i);
      name[i] = '\0';

      return (name);
    }
  return (NULL);
}
