/*
** my_tablen.c for nm-objdump in /home/poussi_w//projects/nm-objdump/utils
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Sat Dec 31 04:29:59 2011 william poussier
** Last update Sat Dec 31 04:32:23 2011 william poussier
*/

#include <stdlib.h>
#include "my_objdump.h"
#include "my_nm.h"

int	my_tablen(char **tab)
{
  int	i;

  i = 0;

  while (tab[i] != NULL)
    i++;

  return (i);
}
