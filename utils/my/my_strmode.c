/*
** my_strmode.c for nm-objdump in /home/poussi_w//projects/nm-objdump/utils/my
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Sat Jan 28 23:59:29 2012 william poussier
** Last update Sun Jan 29 20:46:36 2012 william poussier
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>

char	*my_strmode_usr(mode_t mode, char *p)
{
  if (mode & S_IRUSR)
    *p++ = 'r';
  else
    *p++ = '-';
  if (mode & S_IWUSR)
    *p++ = 'w';
  else
    *p++ = '-';

  switch (mode & (S_IXUSR | S_ISUID))
    {
    case 0:
      *p++ = '-';
      break;
    case S_IXUSR:
      *p++ = 'x';
      break;
    case S_ISUID:
      *p++ = 'S';
      break;
    case S_IXUSR | S_ISUID:
      *p++ = 's';
      break;
    }
  return (p);
}

char	*my_strmode_grp(mode_t mode, char *p)
{
  if (mode & S_IRGRP)
    *p++ = 'r';
  else
    *p++ = '-';
  if (mode & S_IWGRP)
    *p++ = 'w';
  else
    *p++ = '-';

  switch (mode & (S_IXGRP | S_ISGID))
    {
    case 0:
      *p++ = '-';
      break;
    case S_IXGRP:
      *p++ = 'x';
      break;
    case S_ISGID:
      *p++ = 'S';
      break;
    case S_IXGRP | S_ISGID:
      *p++ = 's';
      break;
    }
  return (p);
}

char	*my_strmode_oth(mode_t mode, char *p)
{
  if (mode & S_IROTH)
    *p++ = 'r';
  else
    *p++ = '-';
  if (mode & S_IWOTH)
    *p++ = 'w';
  else
    *p++ = '-';

  switch (mode & (S_IXOTH | S_ISVTX))
    {
    case 0:
      *p++ = '-';
      break;
    case S_IXOTH:
      *p++ = 'x';
      break;
    case S_ISVTX:
      *p++ = 'T';
      break;
    case S_IXOTH | S_ISVTX:
      *p++ = 't';
      break;
    }
  return (p);
}

void	my_strmode(mode_t mode, char *p)
{
  char	*bak;

  bak = my_strmode_usr(mode, p);
  bak = my_strmode_grp(mode, bak);
  bak = my_strmode_oth(mode, bak);

  *bak = '\0';
}
