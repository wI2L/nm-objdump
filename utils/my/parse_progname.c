/*
** parse_progname.c for nm-objdump in /home/poussi_w//projects/nm-objdump/parse
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Fri Dec 23 16:11:03 2011 william poussier
** Last update Fri Dec 23 16:13:21 2011 william poussier
*/

#include <string.h>
#include <stdlib.h>

char	*parse_progname(char *str)
{
  char	*name;

  name = rindex(str, '/');

  if (name != NULL)
    return (name + 1);

  return (str);
}
