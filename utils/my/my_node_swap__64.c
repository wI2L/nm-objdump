/*
** my_node_swap.c for nm-objdump in /home/poussi_w//work/projects/c/nmobjdump-2015-2014s-poussi_w/utils/my
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Fri Mar  9 06:03:42 2012 william poussier
** Last update Fri Mar  9 07:24:23 2012 william poussier
*/

#include <elf.h>
#include "my_nm.h"
#include "utils.h"

void		my_node_swap__64(t_list_64 *e1, t_list_64 *e2)
{
  Elf64_Sym	*sym;
  char		*str;

  sym = e1->sym;
  str = e1->str;
  e1->sym = e2->sym;
  e1->str = e2->str;
  e2->sym = sym;
  e2->str = str;
}
