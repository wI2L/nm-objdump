/*
** get_progname.c for nm-objdump in /home/poussi_w//projects/nm-objdump/parse
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Fri Dec 23 16:01:29 2011 william poussier
** Last update Tue Dec 27 20:10:54 2011 william poussier
*/

#include <stdlib.h>
#include "my_objdump.h"
#include "my_nm.h"

char		*get_progname(char *set)
{
  static char	*name = NULL;

  if (set)
    {
      name = set;
      return (NULL);
    }
  return (name);
}
