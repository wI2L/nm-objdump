/*
** get_exp_pow2.c for nm-objdump in /home/poussi_w//projects/nm-objdump/utils/my
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Tue Jan 31 20:20:13 2012 william poussier
** Last update Tue Jan 31 20:21:04 2012 william poussier
*/

#include "utils.h"

int	get_exp_pow2(unsigned int s)
{
  int	i;

  i = 0;
  while (s > 1)
    {
      s = s / 2;
      i++;
    }
  return (i);
}
