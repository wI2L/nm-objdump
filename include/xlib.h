/*
** xlib.h for nm-objdump in /home/poussi_w//projects/nm-objdump/include
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Dec 22 22:57:44 2011 william poussier
** Last update Sun Jan 29 17:40:27 2012 william poussier
*/

#ifndef		__XLIB_H__
# define	__XLIB_H__

# include	<time.h>
# include	<sys/stat.h>

# define	ERR_XCLOSE	"%s: close error: %s\n"
# define	ERR_XOPEN_STD	"%s: '%s': No such file\n"
# define	ERR_XOPEN_ADVC	"%s: Could not locate '%s': %s\n"
# define	ERR_XFSTAT_STD	"%s: stat error: %s\n"
# define	ERR_XFSTAT_REG	"%s: '%s' is not and ordinary file\n"
# define	ERR_XMALLOC	"%s: fatal error: malloc of size %lu failed: %s"
# define	ERR_XMMAP	"%s: mmap error: %s\n"
# define	ERR_XMUNMAP	"%s: munmap error: %s\n"

# define        ERR_DATE_MSG	"no-time-retrieved"

int		xclose(int);
int		xopen(const char *, int, char *);
int		xfstat(int, struct stat *, char *);
int		xmunmap(void *, size_t);

void            *xmalloc(size_t);
void		*xmmap(void *, size_t, int, int, int, int);

size_t          xstrftime(char *, size_t, const char *, const struct tm *);

#endif		/* !__XLIB_H_ */
