/*
** my_objdump.h for my_objdump in /home/poussi_w//projects/nm-objdump/include
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Dec 22 20:55:46 2011 william poussier
** Last update Fri Mar  9 06:05:45 2012 william poussier
*/

#ifndef		__MY_OBJDUMP_H__
# define	__MY_OBJDUMP_H__

# define	FILE_DEFAULT	"a.out"
# define        CORRUPT		"<corrupt>"
# define	CR		"<corrupt>"

# define	DMP_ADDR_32	"start address 0x%08x\n"
# define	DMP_ADDR_64	"start address 0x%016x\n"

# define	DMP_SCN_HDR_32	"%3d %-13s %08x  %08x  %08x  %08x  2**%u"
# define        DMP_SCN_HDR_64	"%3d %-13s %08lx  %016lx  %016lx  %08lx  2**%u"

# define	DMP_CONTENT	"Contents of section %s:\n"
# define        DMP_OFF_32	"Contents of section %s:  (Starting at file offset: 0x%x)\n"
# define        DMP_OFF_64	"Contents of section %s:  (Starting at file offset: 0x%lx)\n"
# define	DMP_ARCH        "architecture: %s, flags 0x%08x:\n"
# define        DMP             "\n%s:     file format %s-%s\n"

# define	FLAG		"%s%s"

# define	DMP_PRGM_HDR_32	"%8s off    0x%08x vaddr 0x%08x paddr 0x%08x align 2**%u\n"		\
				"         filesz 0x%08x memsz 0x%08x flags %c%c%c"
# define        DMP_PRGM_HDR_64	"%8s off    0x%016lx vaddr 0x%016lx paddr 0x%016lx align 2**%u\n"	\
                                  "         filesz 0x%016lx memsz 0x%016lx flags %c%c%c"

# define	DMP_DYN_STR_32	"  %-20s 0x%08x\n"
# define	DMP_DYN		"  %-20s %s\n"
# define        DMP_DYN_STR_64	"  %-20s 0x%016lx\n"

# define        DMP_VERDEX      "%d 0x%2.2x 0x%8.8x %s\n"
# define	DMP_VERNAUX	"    0x%8.8x 0x%2.2x %2.2d %s\n"

# define	DMP_RELOCS_TT	"RELOCATION RECORDS FOR [%s]\n"

# define	DMP_RELOCS_32	"OFFSET   TYPE              VALUE\n"
# define	DMP_RELOCS_64	"OFFSET           TYPE              VALUE\n"

# define	DMP_REL_OFF_32	"%08x"
# define	DMP_REL_OFF_64	"%016lx"
# define	DMP_REL_TYPE	" %-16s "
# define	DMP_REL_NAME	" %s\n"

# include	"utils.h"

typedef struct	s_objdump_options
{
  int		a;
  int		f;
  int		F;
  int		h;
  int		p;
  int		r;
  int		R;
  int		s;
}		t_objdump_options;

int		objdump_parse_file(char *, t_objdump_options *);
int		objdump_parse_type(void *, char *, t_v_ar *, t_objdump_options *);
int		objdump_parse_isoption(int, char **, t_objdump_options *);

void		objdump_set_options(int, t_objdump_options *);

char		**objdump_parse_options(int, char **, t_objdump_options *);

char		*objdump_parse_segment_type(unsigned int);
char		*objdump_parse_dyn_segment_type(unsigned int, int *);
char		*get_progname(char *);
char		*parse_progname(char *);

void		objdump_usage(void);
void            objdump_parse_file_flags(int);
void            objdump_parse_scn_flags(unsigned int);
void            objdump_parse_flags_print(char *, int);
void		objdump_parse_ar(void *, size_t, t_objdump_options *, char *);

		/* i386 */

int		dump_scn_content__32(void *, t_objdump_options *);
int		dump_scn_content_rules__32(void *, void *, int);
int		get_file_hdr_flags__32(void *, Elf32_Shdr *);
int		dump_scn_hdrs__32(void *);
int		dump_prgm_hdrs__32(void *);
int		dump_scn_hdr_rules__32(void *, Elf32_Shdr *, char *);

unsigned int	get_scn_hdr_flags__32(Elf32_Shdr *);

void		dump_scn_content_split__32(void *, void *);
void		dump_scn_content_hexa__32(void *, unsigned char *, int);
void		dump_scn_content_char__32(void *, unsigned char *, int);
void            dump_header__32(Elf32_Ehdr *, char *, t_v_ar *, t_objdump_options *);
void		dump_content_off__32(t_objdump_options *, char *, Elf32_Shdr *);
void		iterate_over_scns__32(void *, Elf32_Ehdr *, char *, Elf32_Shdr *);
void            dump_scn_hdr__32(Elf32_Shdr *, char *, int);
void		dump_prgm_hdr__32(Elf32_Phdr *, char *);
void		dump_dynamic_scn__32(void *, Elf32_Ehdr *);
void            aff_dynamic_scn__32(Elf32_Dyn *, char *, int);
void		dump_verneed_scn__32(void *, Elf32_Ehdr *);
void		aff_verneed_scn__32(Elf32_Verneed *, char *, Elf32_Vernaux *);
void		dump_verdef_scn__32(void *, Elf32_Ehdr *);
void		aff_verdef_scn__32(Elf32_Verdef *, char *, Elf32_Verdaux *, int);

		/* x86_64 */

int		dump_scn_content__64(void *, t_objdump_options *);
int		dump_scn_content_rules__64(void *, void *, int);
int             get_file_hdr_flags__64(void *, Elf64_Shdr *);
int		dump_scn_hdrs__64(void *);
int		dump_prgm_hdrs__64(void *);
int		dump_scn_hdr_rules__64(void *, Elf64_Shdr *, char *);

unsigned int	get_scn_hdr_flags__64(Elf64_Shdr *);

void		dump_scn_content_split__64(void *, void *);
void		dump_scn_content_hexa__64(void *, unsigned char *, int);
void		dump_scn_content_char__64(void *, unsigned char *, int);
void		dump_header__64(Elf64_Ehdr *, char *, t_v_ar *, t_objdump_options *);
void		dump_content_off__64(t_objdump_options *, char *, Elf64_Shdr *);
void            iterate_over_scns__64(void *, Elf64_Ehdr *, char *, Elf64_Shdr *);
void            dump_scn_hdr__64(Elf64_Shdr *, char *, int);
void		dump_prgm_hdr__64(Elf64_Phdr *, char *);
void		dump_dynamic_scn__64(void *, Elf64_Ehdr *);
void		aff_dynamic_scn__64(Elf64_Dyn *, char *, int);
void		dump_verneed_scn__64(void *, Elf64_Ehdr *);
void		aff_verneed_scn__64(Elf64_Verneed *, char *, Elf64_Vernaux *);
void		dump_verdef_scn__64(void *, Elf64_Ehdr *);
void		aff_verdef_scn__64(Elf64_Verdef *, char *, Elf64_Verdaux *, int);

#endif		/* !__NM_OBJDUMP_H_ */
