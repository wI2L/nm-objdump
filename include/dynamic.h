/*
** dynamic.h for my_objdump in /home/poussi_w//projects/nm-objdump
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Tue Feb 21 16:59:14 2012 william poussier
** Last update Wed Feb 22 23:10:26 2012 william poussier
*/

#ifndef		__DYNAMIC_SEGMENT__
# define	__DYNAMIC_SEGMENT__

# include	"my_objdump.h"

t_dyn_type	dynamic_type[] =
  {
    { DT_PLTRELSZ,		"PLTRELSZ"		},
    { DT_PLTGOT,		"PLTGOT"		},
    { DT_HASH,			"HASH"			},
    { DT_STRTAB,		"STRTAB"		},
    { DT_SYMTAB,		"SYMTAB"		},
    { DT_RELA,			"RELA"			},
    { DT_RELASZ,		"RELASZ"		},
    { DT_RELAENT,		"RELAENT"		},
    { DT_STRSZ,			"STRSZ"			},
    { DT_SYMENT,		"SYMENT"		},
    { DT_INIT,			"INIT"			},
    { DT_FINI,			"FINI"			},
    { DT_SYMBOLIC,		"SYMBOLIC"		},
    { DT_REL,			"REL"			},
    { DT_RELSZ,			"RELSZ"			},
    { DT_RELENT,		"RELENT"		},
    { DT_PLTREL,		"PLTREL"		},
    { DT_DEBUG,			"DEBUG"			},
    { DT_TEXTREL,		"TEXTREL"		},
    { DT_JMPREL,		"JMPREL"		},
    { DT_BIND_NOW,		"BIND_NOW"		},
    { DT_INIT_ARRAY,		"INIT_ARRAY"		},
    { DT_FINI_ARRAY,		"FINI_ARRAY"		},
    { DT_INIT_ARRAYSZ,		"INIT_ARRAYSZ"		},
    { DT_FINI_ARRAYSZ,		"FINI_ARRAYSZ"		},
    { DT_FLAGS,			"FLAGS"			},
    { DT_PREINIT_ARRAY,		"PREINIT_ARRAY"		},
    { DT_PREINIT_ARRAYSZ,	"PREINIT_ARRAYSZ"	},
    { DT_CHECKSUM,		"CHECKSUM"		},
    { DT_PLTPADSZ,		"PLTPADSZ"		},
    { DT_MOVEENT,		"MOVEENT"		},
    { DT_MOVESZ,		"MOVESZ"		},
    { DT_POSFLAG_1,		"POSFLAG_1"		},
    { DT_SYMINSZ,		"SYMINSZ"		},
    { DT_SYMINENT,		"SYMINENT"		},
    { DT_PLTPAD,		"PLTPAD"		},
    { DT_MOVETAB,		"MOVETAB"		},
    { DT_SYMINFO,		"SYMINFO"		},
    { DT_RELACOUNT,		"RELACOUNT"		},
    { DT_RELCOUNT,		"RELCOUNT"		},
    { DT_FLAGS_1,		"FLAGS_1"		},
    { DT_VERSYM,		"VERSYM"		},
    { DT_VERDEF,		"VERDEF"		},
    { DT_VERDEFNUM,		"VERDEFNUM"		},
    { DT_VERNEED,		"VERNEED"		},
    { DT_VERNEEDNUM,		"VERNEEDNUM"		},
    { DT_GNU_HASH,		"GNU_HASH"		},
    { DT_NEEDED,		"NEEDED"		},
    { DT_SONAME,		"SONAME"		},
    { DT_RPATH,			"RPATH"			},
    { DT_CONFIG,		"CONFIG"		},
    { DT_DEPAUDIT,		"DEPAUDIT"		},
    { DT_FILTER,		"FILTER"		},
    { 0, NULL						}
  };

#endif		/* !__DYNAMIC_SEGMENT_ */
