/*
** my_nm.h for my_nm in /home/poussi_w//projects/nm-objdump/include
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Tue Dec 27 16:57:14 2011 william poussier
** Last update Fri Mar  9 07:29:22 2012 william poussier
*/

#ifndef			__MY_NM_H_
# define		__MY_NM_H_

# define		FILE_DEFAULT		"a.out"
# define		PRINT_FILENAME		"\n%s:\n"
# define		NO_SYMBOLS		"%s: %s: no symbols\n"

# define		SYM64_STD_SIZE		"%016lx %016lx %c %s\n"
# define		SYM64_STD_NOSIZE	"%016lx %c %s\n"

# define                SYM32_STD_SIZE		"%08x %08x %c %s\n"
# define                SYM32_STD_NOSIZE	"%08x %c %s\n"

# define		SYM32_UNDEF		"%10c %s\n"
# define		SYM64_UNDEF		"%18c %s\n"

# include		"utils.h"

typedef struct		s_nm_options
{
  int			A;
  int			d;
  int			D;
  int			n;
  int			p;
  int			r;
  int			S;
  int			u;
  char			*filename;
}			t_nm_options;

			/* i386 */

typedef	struct		s_list_32
{
  Elf32_Sym		*sym;
  char			*str;
  struct s_list_32	*next;
}			t_list_32;

			/* x86_64 */

typedef	struct		s_list_64
{
  Elf64_Sym		*sym;
  char			*str;
  struct s_list_64	*next;
}			t_list_64;

int             nm_parse_file(char *, t_nm_options *);
int             nm_parse_type(void *, char *);
int		nm_parse_isoption(int, char **, t_nm_options *);

t_nm_options	*nm_get_options(void);

void		nm_usage(void);
void            nm_parse_ar(void *, size_t, char *, t_nm_options *);
void		my_node_swap__64(t_list_64 *, t_list_64 *);
void		my_node_swap__32(t_list_32 *, t_list_32 *);

char		**nm_parse_options(int, char **, t_nm_options *);
char            *get_progname(char *);
char            *parse_progname(char *);

		/* i386 */

void		dump_symbols__32(void *, char *);
void		dump_print__32(void *, int, Elf32_Shdr *, Elf32_Shdr *);
void		list_add__32(Elf32_Sym *, char *, t_list_32 **);
void		list_add_nosort__32(t_list_32 *, t_list_32 **);
void		list_alphasort__32(t_list_32 **);
void		list_numsort__32(t_list_32 **);
void		list_print__32(void *, t_list_32 **);
void		list_free__32(t_list_32 **);
void		list_rev__32(t_list_32 **);
void		print_symbol__32(void *, t_list_32 *, t_nm_options *);

int		dump_search__32(void *, Elf32_Shdr **, Elf32_Shdr **);

char		get_symbtype__32(void *, Elf32_Sym *);
char		decode_section_type__32(Elf32_Shdr *);
char		decode_symclass__32(void *, Elf32_Sym *);
char		iterate_symtype_over_scns__32(char *);

		/* x86_64 */

void		dump_symbols__64(void *, char *);
void		dump_print__64(void *, int, Elf64_Shdr *, Elf64_Shdr *);
void		list_add__64(Elf64_Sym *, char *, t_list_64 **);
void		list_add_nosort__64(t_list_64 *, t_list_64 **);
void		list_alphasort__64(t_list_64 **);
void		list_numsort__64(t_list_64 **);
void		list_print__64(void *, t_list_64 **);
void		list_free__64(t_list_64 **);
void		list_rev__64(t_list_64 **);
void		print_symbol__64(void *, t_list_64 *, t_nm_options *);

int		dump_search__64(void *, Elf64_Shdr **, Elf64_Shdr **);

char		get_symbtype__64(void *, Elf64_Sym *);
char            decode_section_type__64(Elf64_Shdr *);
char            decode_symclass__64(void *, Elf64_Sym *);
char		iterate_symtype_over_scns__64(char *);

#endif		/* !__MY_NM_H_ */
