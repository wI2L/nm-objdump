/*
** utils.h for nm-objdump in /home/poussi_w//projects/nm-objdump/include
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Fri Dec 23 01:24:51 2011 william poussier
** Last update Fri Mar  9 06:06:30 2012 william poussier
*/

#ifndef		__UTILS_H_
# define	__UTILS_H_

# include	<ar.h>
# include	<elf.h>
# include	<time.h>
# include	<sys/types.h>

# if defined	(__x86_64__)

typedef		Elf64_Ehdr	Elf_Ehdr;
# endif

# if defined    (__i386__)

typedef		Elf32_Ehdr	Elf_Ehdr;
# endif

# define	AR_SIZE		10
# define	AR_MODE		12
# define        AR_NAME         16
# define	AR_DATE		64
# define        AR_BUFFER       512

typedef         struct ar_hdr   t_ar;

typedef		struct s_v_ar
{
  char		*name;
  time_t	date;
  unsigned int	uid;
  unsigned int	gid;
  unsigned int	mode;
  unsigned long	size;
}		t_v_ar;

typedef		struct s_dyn_type
{
  unsigned int	dt_type;
  char		*dt_name;
}		t_dyn_type;

typedef		struct s_syms_type
{
  char		*scn;
  char		type;
}		t_syms_type;

# define        BAD_FORMAT      "%s: %s: File format not recognized\n"
# define	AR_MALFORMED	"%s: %s: Malformed archive\n"
# define	AR_DATE_FORMAT	"%b %d %H:%M %Y"

# define	AR_DECL		"In archive %s:\n"
# define	AR_MEMBER_NAME	"\n%s:\n"
# define	AR_FILENAME	"%s:"
# define        AR_NODATA       "nodata"
# define	AR_END_CHAR	'/'
# define	AR_INDEX	"/"
# define	AR_STRTAB	"//"

# define	HAS_RELOC	0x1
# define	EXEC_P		0x2
# define	HAS_LINENO	0x4
# define	HAS_DEBUG	0x8
# define	HAS_SYMS	0x10
# define	HAS_LOCALS	0x20
# define	DYNAMIC		0x40
# define	WP_TEXT		0x80
# define	D_PAGED		0x100

# define	SEC_NO_FLAGS		0x000
# define	SEC_ALLOC		0x001
# define	SEC_LOAD		0x002
# define	SEC_RELOC		0x004
# define	SEC_READONLY		0x008
# define	SEC_CODE		0x010
# define	SEC_DATA		0x020
# define	SEC_ROM			0x040
# define	SEC_CONSTRUCTOR		0x080
# define	SEC_HAS_CONTENTS	0x100
# define	SEC_NEVER_LOAD		0x200
# define	SEC_THREAD_LOCAL	0x400
# define	SEC_HAS_GOT_REF		0x800
# define	SEC_IS_COMMON		0x1000
# define	SEC_DEBUGGING		0x2000
# define	SEC_IN_MEMORY		0x4000
# define	SEC_EXCLUDE		0x8000
# define	SEC_SORT_ENTRIES	0x10000
# define	SEC_LINK_ONCE		0x20000
# define	SEC_LINK_DUPLICATES	0xc0000
# define	SEC_KEEP		0x200000
# define	SEC_SMALL_DATA		0x400000
# define	SEC_MERGE		0x800000
# define	SEC_STRINGS		0x1000000
# define	SEC_GROUP		0x2000000

char		*elf_getarchi(int);
char		*elf_getclass(int);
char		*elf_gettarget(int);
char		*ar_getmember_name(char *, char *);
char		*elf_getshstrtab__32(void *);
char		*elf_getshstrtab__64(void *);
char		*elf_getstrtab__32(void *);
char		*elf_getstrtab__64(void *);

int		elf_check(Elf_Ehdr *);
int		ar_check(void *);
int		my_tablen(char **);
int		get_exp_pow2(unsigned int);

void		ar_extract_hdata(t_ar *, t_v_ar *);
void		ar_aff_hdata(t_v_ar *);
void		ar_check_member(t_ar *, char *);
void		my_strmode(mode_t, char *);

void		*elf_getscn_by_type__32(void *, unsigned int);
void		*elf_getscn_by_name__32(void *, char *);
void		*elf_getscn_by_type__64(void *, unsigned int);
void		*elf_getscn_by_name__64(void *, char *);

void            *ar_getmember(char *, void *, size_t, char *, int);
void            *ar_getmember_data(char *, void *, unsigned int *, char *, unsigned int *);

void		*ar_getmember_wh(char *, void *, size_t, t_v_ar *, int);
void		*ar_getmember_data_wh(char *, void *, unsigned int *, t_v_ar *, unsigned int *);
void            *ar_parse_data(void *, unsigned int *, char *, char *);

#endif		/* !__UTILS_H_ */
