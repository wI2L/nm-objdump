/*
** syms.h for my_nm in /home/poussi_w//projects/nm-objdump/include
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb 22 22:19:32 2012 william poussier
** Last update Fri Mar  9 04:28:23 2012 william poussier
*/

#ifndef		__SYMS_TYPE_H__
# define	__SYMS_TYPE_H__

# include	"my_nm.h"

t_syms_type		symst_db[] =
  {
    { ".bss",		'b' },
    { "code",		't' },
    { ".data",		'd' },
    { "*DEBUG*",	'N' },
    { ".debug",		'N' },
    { ".drectve",	'i' },
    { ".edata",		'e' },
    { ".fini",		't' },
    { ".idata",		'i' },
    { ".init",		't' },
    { ".pdata",		'p' },
    { ".rdata",		'r' },
    { ".rodata",	'r' },
    { ".sbss",		's' },
    { ".scommon",	'c' },
    { ".sdata",		'g' },
    { ".text",		't' },
    { ".vars",		'd' },
    { ".zerovars",	'b' }
  };

#endif		/* !__SYMS_TYPE_HH_ */
