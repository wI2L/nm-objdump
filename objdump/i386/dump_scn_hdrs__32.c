/*
** dump_scn_hdrs__32.c for my_objdump in /home/poussi_w//projects/nm-objdump/objdump/i386
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Tue Jan 31 17:45:39 2012 william poussier
** Last update Mon Feb 20 17:29:20 2012 william poussier
*/

#include <stdio.h>
#include "my_objdump.h"

int		dump_scn_hdrs__32(void *buf)
{
  Elf32_Ehdr	*elf;
  Elf32_Shdr	*scn;
  char		*shstrtab;

  elf = buf;
  shstrtab = elf_getshstrtab__32(buf);

  printf("Sections:\n");
  printf("Idx Name          Size      VMA       LMA       File off  Algn\n");

  iterate_over_scns__32(buf, elf, shstrtab, scn);

  printf("\n");

  return (0);
}
