/*
** dump_scn_split_string.c for my_objdump in /home/poussi_w//projects/nm-objdump/dump
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Dec 22 22:34:49 2011 william poussier
** Last update Wed Feb 22 17:51:19 2012 william poussier
*/

#include <elf.h>
#include <ctype.h>
#include <stdio.h>
#include "my_objdump.h"

void		dump_scn_content_char__32(void *s, unsigned char *str, int size)
{
  Elf32_Shdr	*scn;
  unsigned int	i;

  i = 0;
  scn = s;

  while (i < 16)
    {
      if ((size + i) < scn->sh_size)
	{
	  if (isprint(str[size + i]))
	    printf("%c", str[size + i]);
	  else
	    printf(".");
	}
      else
	printf(" ");
      i++;
    }
}
