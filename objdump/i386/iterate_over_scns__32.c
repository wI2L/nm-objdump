/*
** iterate_over_scns__32.c for my_ojdump in /home/poussi_w//projects/nm-objdump/objdump/i386
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Tue Jan 31 21:46:04 2012 william poussier
** Last update Thu Feb 23 20:45:28 2012 william poussier
*/

#include <string.h>
#include "my_objdump.h"

void		iterate_over_scns__32(void *buf, Elf32_Ehdr *elf,
				      char *strtab, Elf32_Shdr *scn)
{
  int		i;
  int		j;
  unsigned int	flags;

  i = 0;
  j = 0;

  while (i < elf->e_shnum)
    {
      scn = buf + elf->e_shoff + (elf->e_shentsize * i);
      flags = get_scn_hdr_flags__32(scn);

      if (!dump_scn_hdr_rules__32(buf, scn, strtab))
	{
	  dump_scn_hdr__32(scn, strtab, j);
	  objdump_parse_scn_flags(flags);
	  j++;
	}
      i++;
    }
}
