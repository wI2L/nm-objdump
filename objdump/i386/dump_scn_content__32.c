/*
** dump_scn.c for my_objdump in /home/poussi_w//projects/nm-objdump/dump
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Dec 22 22:20:23 2011 william poussier
** Last update Wed Feb 22 17:40:57 2012 william poussier
*/

#include <elf.h>
#include <stdio.h>
#include "my_objdump.h"

int		dump_scn_content__32(void *buf, t_objdump_options *op)
{
  Elf32_Ehdr	*elf;
  Elf32_Shdr	*scn;
  char		*shstrtab;
  int		i;

  i = 0;
  elf = buf;
  shstrtab = elf_getshstrtab__32(buf);

  while (i < elf->e_shnum)
    {
      scn = buf + elf->e_shoff + (elf->e_shentsize * i);

      if (!dump_scn_content_rules__32(elf, scn, i))
	{
	  dump_content_off__32(op, shstrtab, scn);
	  dump_scn_content_split__32(buf, scn);
	}
      i++;
    }
  return (1);
}
