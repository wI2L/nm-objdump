/*
** dump_prgm_hdrs__32.c for my_objdump in /home/poussi_w//projects/nm-objdump/objdump/i386
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb  1 08:43:19 2012 william poussier
** Last update Mon Feb 20 16:13:57 2012 william poussier
*/

#include <elf.h>
#include <stdio.h>
#include "my_objdump.h"

int		dump_prgm_hdrs__32(void *buf)
{
  Elf32_Ehdr	*elf;
  Elf32_Phdr	*phdr;
  char		*pt;
  int		i;

  i = -1;
  elf = buf;

  if (elf->e_phnum != 0)
    printf("Program Header:\n");

  while (++i < elf->e_phnum)
    {
      phdr = buf + elf->e_phoff + (elf->e_phentsize * i);
      pt = objdump_parse_segment_type(phdr->p_type);
      dump_prgm_hdr__32(phdr, pt);
    }
  dump_dynamic_scn__32(buf, elf);
  dump_verdef_scn__32(buf, elf);
  dump_verneed_scn__32(buf, elf);

  if (elf->e_phnum != 0)
    printf("\n");

  return (0);
}
