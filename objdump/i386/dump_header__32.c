/*
** dump_header.c for my_objdump in /home/poussi_w//projects/nm-objdump/dump
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Fri Dec 23 00:49:48 2011 william poussier
** Last update Mon Feb 20 18:25:09 2012 william poussier
*/

#include <elf.h>
#include <stdio.h>
#include "my_objdump.h"

void		dump_header__32(Elf32_Ehdr *elf, char *filename,
				t_v_ar *h_ar, t_objdump_options *op)
{
  Elf32_Shdr	*sec;
  int		flags;

  flags = get_file_hdr_flags__32(elf, sec);

  printf(DMP, filename, elf_getclass(elf->e_ident[EI_CLASS]),
	 elf_getarchi(elf->e_machine));

  if (!op->f && !op->a)
    printf("\n");
  if (op->a)
    ar_aff_hdata(h_ar);
  if (op->a && !op->f)
    printf("\n");

  if (op->f == 1)
    {
      printf(DMP_ARCH, elf_gettarget(elf->e_machine), flags);
      objdump_parse_file_flags(flags);
      printf(DMP_ADDR_32, (unsigned int) elf->e_entry);
      printf("\n");
    }
}
