/*
** dump_scn_rules.c for my_objdump in /home/poussi_w//projects/nm-objdump/dump
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Dec 22 22:23:53 2011 william poussier
** Last update Wed Feb 22 17:12:47 2012 william poussier
*/

#include <elf.h>
#include <string.h>
#include "my_objdump.h"

int		dump_scn_content_rules__32(void *buf, void *s, int nb)
{
  Elf32_Ehdr	*elf;
  Elf32_Shdr	*scn;
  char		*scnname;

  elf = buf;
  scn = s;
  scnname = elf_getshstrtab__32(buf) + scn->sh_name;

  if (scn->sh_type == SHT_NULL || scn->sh_type == SHT_NOBITS
      || nb == elf->e_shstrndx
      || !scn->sh_size)
    return (1);

  if (elf->e_type == ET_REL && scn->sh_type == SHT_REL)
    return (1);

  if (strcmp(scnname, ".symtab") == 0
      || strcmp(scnname, ".strtab") == 0
      || strcmp(scnname, ".shstrtab") == 0)
    {
      return (1);
    }
  return (0);
}
