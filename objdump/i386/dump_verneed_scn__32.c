/*
** dump_verneed_scn__32.c for my_objdump in /home/poussi_w//projects/nm-objdump
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb  2 22:56:02 2012 william poussier
** Last update Mon Feb 20 18:20:19 2012 william poussier
*/

#include <elf.h>
#include <stdio.h>
#include <stdlib.h>
#include "my_objdump.h"

void		dump_verneed_scn__32(void *buf, Elf32_Ehdr *elf)
{
  Elf32_Shdr	*dynscn;
  Elf32_Verneed	*vern;
  Elf32_Vernaux	*verna;
  Elf32_Shdr	*scn;
  char		*dynstr;

  dynstr = NULL;
  vern = NULL;
  verna = NULL;

  if ((scn = elf_getscn_by_type__32(buf, SHT_GNU_verneed)) != NULL)
    vern = buf + scn->sh_offset;
  if ((scn = elf_getscn_by_type__32(buf, SHT_DYNSYM)) != NULL)
    {
      dynscn = buf + elf->e_shoff + (scn->sh_link * sizeof(Elf32_Shdr));
      dynstr = buf + dynscn->sh_offset;
    }

  if (vern != NULL && dynstr != NULL)
    {
      printf("\nVersion References:\n");
      aff_verneed_scn__32(vern, dynstr, verna);
    }
}

void		aff_verneed_scn__32(Elf32_Verneed *vern, char *dynstr,
				    Elf32_Vernaux *verna)
{
  int		cont;
  int		i;

  do
    {
      printf("  required from %s:\n",
	     vern->vn_file ? (dynstr + vern->vn_file) : CORRUPT);
      i = 0;
      verna = (Elf32_Vernaux *)((char *) vern + vern->vn_aux);

      while (i < vern->vn_cnt)
	{
	  printf(DMP_VERNAUX,
		 verna->vna_hash,
		 verna->vna_flags,
		 verna->vna_other,
		 verna->vna_name ? (dynstr + verna->vna_name) : CORRUPT);
	  verna = (Elf32_Vernaux *)((char *) verna + verna->vna_next);
	  i++;
	}
      cont = vern->vn_next != 0;
      vern = (Elf32_Verneed *)((char *) vern + vern->vn_next);
    }
  while (cont);
}
