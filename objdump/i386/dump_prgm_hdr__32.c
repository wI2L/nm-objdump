/*
** dump_prgm_hdr__32.c for my_objdump in /home/poussi_w//projects/nm-objdump/objdump/i386
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb  1 09:56:52 2012 william poussier
** Last update Wed Feb  1 12:36:33 2012 william poussier
*/

#include <stdio.h>
#include "my_objdump.h"

void	dump_prgm_hdr__32(Elf32_Phdr *phdr, char *pt)
{
  char	buffer[32];

  if (pt == NULL)
    {
      sprintf(buffer, "0x%x", phdr->p_type);
      pt = buffer;
    }

  printf(DMP_PRGM_HDR_32,
	 pt,
	 phdr->p_offset,
	 phdr->p_vaddr, phdr->p_paddr,
	 get_exp_pow2(phdr->p_align),
	 phdr->p_filesz, phdr->p_memsz,

	 (phdr->p_flags & PF_R) != 0 ? 'r' : '-',
	 (phdr->p_flags & PF_W) != 0 ? 'w' : '-',
	 (phdr->p_flags & PF_X) != 0 ? 'x' : '-');

  if ((phdr->p_flags &~ (unsigned) (PF_R | PF_W | PF_X)) != 0)
    printf(" %x", phdr->p_flags &~ (unsigned) (PF_R | PF_W | PF_X));

  printf("\n");
}
