/*
** dump_scn_split_hexa.c for my_objdump in /home/poussi_w//projects/nm-objdump/dump
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Dec 22 22:37:31 2011 william poussier
** Last update Wed Feb 22 17:34:32 2012 william poussier
*/

#include <elf.h>
#include <stdio.h>
#include "my_objdump.h"

void		dump_scn_content_hexa__32(void *s, unsigned char *str, int size)
{
  Elf32_Shdr	*scn;
  unsigned int	i;

  i = 0;
  scn = s;

  while (i < 16)
    {
      if ((size + i) < scn->sh_size)
	printf("%02x", str[size + i]);
      else
	printf("  ");
      if ((i % 4) == 3)
	printf(" ");
      i++;
    }
}
