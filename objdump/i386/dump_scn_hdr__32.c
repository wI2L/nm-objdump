/*
** dump_scn_hdr__32.c for my_objdump in /home/poussi_w//projects/nm-objdump/objdump/i386
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb  1 08:26:27 2012 william poussier
** Last update Wed Feb  1 13:08:31 2012 william poussier
*/

#include <stdio.h>
#include "my_objdump.h"

void	dump_scn_hdr__32(Elf32_Shdr *scn, char *strtab, int i)
{
  printf(DMP_SCN_HDR_32,
	 i,
	 strtab + scn->sh_name,
	 scn->sh_size,
	 scn->sh_addr,
	 scn->sh_addr,
	 scn->sh_offset,
	 get_exp_pow2(scn->sh_addralign));

  printf("\n                  ");
}
