/*
** dump_dynamic_scn__32.c for my_objdump in /home/poussi_w//projects/nm-objdump/objdump/i386
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb  1 22:11:31 2012 william poussier
** Last update Mon Feb 20 18:49:49 2012 william poussier
*/

#include <elf.h>
#include <stdio.h>
#include <stdlib.h>
#include "my_objdump.h"
#include "utils.h"
#include "xlib.h"

void		dump_dynamic_scn__32(void *buf, Elf32_Ehdr *elf)
{
  Elf32_Shdr	*scn;
  Elf32_Shdr    *dynscn;
  Elf32_Dyn	*dynamic;
  char		*dynstr;
  int		nb_dts;

  dynstr = NULL;
  dynamic = NULL;

  if ((scn = elf_getscn_by_type__32(buf, SHT_DYNAMIC)) != NULL)
    {
      nb_dts = scn->sh_size / sizeof (Elf32_Dyn);
      dynamic = xmalloc (nb_dts * sizeof (Elf32_Dyn));
      dynamic = buf + scn->sh_offset;
    }
  if ((scn = elf_getscn_by_type__32(buf, SHT_DYNSYM)) != NULL)
    {
      dynscn = buf + elf->e_shoff + (scn->sh_link * sizeof(Elf32_Shdr));
      dynstr = buf + dynscn->sh_offset;
    }
  if (dynamic != NULL && dynstr != NULL)
    {
      printf("\nDynamic Section:\n");
      aff_dynamic_scn__32(dynamic, dynstr, nb_dts);
    }
}

void		aff_dynamic_scn__32(Elf32_Dyn *dynamic, char *dynstr, int nb_dts)
{
  int		i;
  int		stringp;
  char		*name;
  char		ab[20];

  i = 0;

  while (i < nb_dts)
    {
      if (dynamic[i].d_tag != DT_NULL)
	{
	  name = objdump_parse_dyn_segment_type(dynamic[i].d_tag, &stringp);

	  if (name == NULL)
	    {
	      sprintf(ab, "0x%x", (unsigned) dynamic[i].d_tag);
	      name = ab;
	    }
	  (!stringp) ? printf(DMP_DYN_STR_32, name, (dynamic[i].d_un.d_val)) :
	    printf(DMP_DYN, name, (dynstr + dynamic[i].d_un.d_val));

	  stringp = 0;
	}
      i++;
    }
}
