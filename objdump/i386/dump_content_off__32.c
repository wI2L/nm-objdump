/*
** dump_content_off__32.c for nm-objdump in /home/poussi_w//projects/nm-objdump/objdump/i386
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Sun Jan 29 22:37:11 2012 william poussier
** Last update Tue Feb 21 09:53:01 2012 william poussier
*/

#include <stdio.h>
#include "my_objdump.h"

void	dump_content_off__32(t_objdump_options *op, char *str, Elf32_Shdr *s)
{
  if (op->F)
    printf(DMP_OFF_32, str + s->sh_name, s->sh_offset);
  else
    printf(DMP_CONTENT, str + s->sh_name);
}
