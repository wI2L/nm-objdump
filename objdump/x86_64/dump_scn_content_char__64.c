/*
** dump_scn_content_char__64.c for nm-objdump in /home/poussi_w//projects/nm-objdump/objdump/x86_64
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb 22 18:02:09 2012 william poussier
** Last update Wed Feb 22 18:02:41 2012 william poussier
*/

#include <elf.h>
#include <ctype.h>
#include <stdio.h>
#include "my_objdump.h"

void		dump_scn_content_char__64(void *s, unsigned char *str, int nb)
{
  Elf64_Shdr	*scn;
  unsigned int	i;

  i = 0;
  scn = s;

  while (i < 16)
    {
      if ((nb + i) < scn->sh_size)
	{
	  if (isprint(str[nb + i]))
	    printf("%c", str[nb + i]);
	  else
	    printf(".");
	}
      else
	printf(" ");
      i++;
    }
}
