/*
** dump_dynamic_scn__64.c for my_onjdump in /home/poussi_w//projects/nm-objdump/objdump/x86_64
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb  2 05:51:54 2012 william poussier
** Last update Tue Feb 21 09:29:38 2012 william poussier
*/

#include <elf.h>
#include <stdio.h>
#include <stdlib.h>
#include "my_objdump.h"
#include "utils.h"
#include "xlib.h"

void		dump_dynamic_scn__64(void *buf, Elf64_Ehdr *elf)
{
  Elf64_Shdr	*scn;
  Elf64_Shdr    *dynscn;
  Elf64_Dyn	*dynamic;
  char		*dynstr;
  int		nb_dts;

  dynstr = NULL;
  dynamic = NULL;

  if ((scn = elf_getscn_by_type__64(buf, SHT_DYNAMIC)) != NULL)
    {
      nb_dts = scn->sh_size / sizeof (Elf64_Dyn);
      dynamic = xmalloc (nb_dts * sizeof (Elf64_Dyn));
      dynamic = buf + scn->sh_offset;
    }
  if ((scn = elf_getscn_by_type__64(buf, SHT_DYNSYM)) != NULL)
    {
      dynscn = buf + elf->e_shoff + (scn->sh_link * sizeof(Elf64_Shdr));
      dynstr = buf + dynscn->sh_offset;
    }
  if (dynamic != NULL && dynstr != NULL)
    {
      printf("\nDynamic Section:\n");
      aff_dynamic_scn__64(dynamic, dynstr, nb_dts);
    }
}

void		aff_dynamic_scn__64(Elf64_Dyn *dynamic, char *dynstr, int nb_dts)
{
  int		i;
  int		stringp;
  char		*name;
  char		ab[20];

  i = 0;

  while (i < nb_dts)
    {
      if (dynamic[i].d_tag != DT_NULL)
	{
	  name = objdump_parse_dyn_segment_type(dynamic[i].d_tag, &stringp);

	  if (name == NULL)
	    {
	      sprintf(ab, "0x%lx", (unsigned long) dynamic[i].d_tag);
	      name = ab;
	    }
	  (!stringp) ? printf(DMP_DYN_STR_64, name, (dynamic[i].d_un.d_val)) :
	    printf(DMP_DYN, name, (dynstr + dynamic[i].d_un.d_val));

	  stringp = 0;
	}
      i++;
    }
}
