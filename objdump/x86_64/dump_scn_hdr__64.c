/*
** dump_scn_hdr__64.c for my_objdump in /home/poussi_w//projects/nm-objdump/objdump/x86_64
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb  1 08:27:10 2012 william poussier
** Last update Wed Feb  1 13:08:45 2012 william poussier
*/

#include <stdio.h>
#include "my_objdump.h"

void	dump_scn_hdr__64(Elf64_Shdr *scn, char *strtab, int i)
{
  printf(DMP_SCN_HDR_64,
	 i,
	 strtab + scn->sh_name,
	 scn->sh_size,
	 scn->sh_addr,
	 scn->sh_addr,
	 scn->sh_offset,
	 get_exp_pow2(scn->sh_addralign));

  printf("\n                  ");
}
