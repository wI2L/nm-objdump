/*
** dump_scn_split.c for my_objdump in /home/poussi_w//projects/nm-objdump/dump
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Dec 22 22:31:21 2011 william poussier
** Last update Wed Feb 22 18:03:30 2012 william poussier
*/

#include <elf.h>
#include <stdio.h>
#include "my_objdump.h"

void		dump_scn_content_split__64(void *buf, void *s)
{
  Elf64_Shdr	*scn;
  unsigned char	*str;
  unsigned int	i;

  i = 0;
  scn = s;
  str = buf + scn->sh_offset;

  while (i < scn->sh_size)
    {
      printf(" %04x ", (unsigned int) scn->sh_addr + i);
      dump_scn_content_hexa__64(scn, str, i);
      printf(" ");
      dump_scn_content_char__64(scn, str, i);
      printf("\n");
      i += 16;
    }
}
