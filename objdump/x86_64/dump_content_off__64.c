/*
** dump_content_off__64.c for nm-objdump in /home/poussi_w//projects/nm-objdump/objdump/x86_64
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Sun Jan 29 22:39:39 2012 william poussier
** Last update Tue Feb 21 09:52:49 2012 william poussier
*/

#include <stdio.h>
#include "my_objdump.h"

void	dump_content_off__64(t_objdump_options *op, char *str, Elf64_Shdr *s)
{
  if (op->F)
    printf(DMP_OFF_64, str + s->sh_name, s->sh_offset);
  else
    printf(DMP_CONTENT, str + s->sh_name);
}
