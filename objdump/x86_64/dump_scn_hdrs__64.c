/*
** dump_scn_hdrs__64.c for my_objdump in /home/poussi_w//projects/nm-objdump
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb  1 00:22:45 2012 william poussier
** Last update Tue Feb 21 09:53:34 2012 william poussier
*/

#include <stdio.h>
#include "my_objdump.h"

int		dump_scn_hdrs__64(void *buf)
{
  Elf64_Ehdr	*elf;
  Elf64_Shdr	*scn;
  char		*shstrtab;

  elf = buf;
  shstrtab = elf_getshstrtab__64(buf);

  printf("Sections:\n");
  printf("Idx Name          Size      VMA               "
	 "LMA               File off  Algn\n");

  iterate_over_scns__64(buf, elf, shstrtab, scn);

  printf("\n");

  return (0);
}
