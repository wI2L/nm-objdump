/*
** iterate_over_scns__64.c for my_objdump in /home/poussi_w//projects/nm-objdump
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb  1 00:18:41 2012 william poussier
** Last update Thu Feb 23 20:45:36 2012 william poussier
*/

#include <string.h>
#include "my_objdump.h"

void		iterate_over_scns__64(void *buf, Elf64_Ehdr *elf,
				      char *strtab, Elf64_Shdr *scn)
{
  int		i;
  int		j;
  unsigned int	flags;

  i = 0;
  j = 0;

  while (i < elf->e_shnum)
    {
      scn = buf + elf->e_shoff + (elf->e_shentsize * i);
      flags = get_scn_hdr_flags__64(scn);

      if (!dump_scn_hdr_rules__64(buf, scn, strtab))
	{
	  dump_scn_hdr__64(scn, strtab, j);
	  objdump_parse_scn_flags(flags);
	  j++;
	}
      i++;
    }
}
