/*
** dump_verneed_scn__64.c for my_objdump in /home/poussi_w//projects/nm-objdump
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Fri Feb  3 01:47:43 2012 william poussier
** Last update Tue Feb 21 09:39:14 2012 william poussier
*/

#include <elf.h>
#include <stdio.h>
#include <stdlib.h>
#include "my_objdump.h"

void		dump_verneed_scn__64(void *buf, Elf64_Ehdr *elf)
{
  Elf64_Shdr	*dynscn;
  Elf64_Verneed	*vern;
  Elf64_Vernaux	*verna;
  Elf64_Shdr	*scn;
  char		*dynstr;

  dynstr = NULL;
  vern = NULL;
  verna = NULL;

  if ((scn = elf_getscn_by_type__64(buf, SHT_GNU_verneed)) != NULL)
    vern = buf + scn->sh_offset;
  if ((scn = elf_getscn_by_type__64(buf, SHT_DYNSYM)) != NULL)
    {
      dynscn = buf + elf->e_shoff + (scn->sh_link * sizeof(Elf64_Shdr));
      dynstr = buf + dynscn->sh_offset;
    }

  if (vern != NULL && dynstr != NULL)
    {
      printf("\nVersion References:\n");
      aff_verneed_scn__64(vern, dynstr, verna);
    }
}

void		aff_verneed_scn__64(Elf64_Verneed *vern, char *dynstr,
				    Elf64_Vernaux *verna)
{
  int		cont;
  int		i;

  do
    {
      printf("  required from %s:\n",
	     vern->vn_file ? (dynstr + vern->vn_file) : CORRUPT);
      i = 0;
      verna = (Elf64_Vernaux *)((char *) vern + vern->vn_aux);

      while (i < vern->vn_cnt)
	{
	  printf(DMP_VERNAUX,
		 verna->vna_hash,
		 verna->vna_flags,
		 verna->vna_other,
		 verna->vna_name ? (dynstr + verna->vna_name) : CORRUPT);
	  verna = (Elf64_Vernaux *)((char *) verna + verna->vna_next);
	  i++;
	}
      cont = vern->vn_next != 0;
      vern = (Elf64_Verneed *)((char *) vern + vern->vn_next);
    }
  while (cont);
}
