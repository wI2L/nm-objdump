/*
** dump_scn_hdr_rules__64.c for my_objdump in /home/poussi_w//projects/nm-objdump
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 23 20:37:23 2012 william poussier
** Last update Thu Feb 23 20:45:10 2012 william poussier
*/

#include <elf.h>
#include <string.h>
#include "my_objdump.h"

int		dump_scn_hdr_rules__64(void *buf, Elf64_Shdr *scn, char *strtab)
{
  Elf64_Ehdr	*elf;

  elf = buf;

  if ((scn->sh_type == SHT_REL || scn->sh_type == SHT_RELA)
      && (elf->e_type == ET_REL))
    {
      return (1);
    }
  if (scn->sh_type == SHT_NULL
      || scn->sh_type == SHT_SYMTAB
      || scn->sh_type == SHT_STRTAB
      || strcmp(scn->sh_name + strtab, ".dynstr") == 0)
    {
      return (1);
    }
  return (0);
}
