/*
** get_file_hdr_flags__64.c for my_objdump in /home/poussi_w//projects/nm-objdump
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Sun Dec 25 11:07:18 2011 william poussier
** Last update Sat Feb  4 16:36:20 2012 william poussier
*/

#include <elf.h>
#include "my_objdump.h"

int		get_file_hdr_flags__64(void *buf, Elf64_Shdr *sec)
{
  Elf64_Ehdr	*elf;
  int		flags;
  int		i;

  elf = buf;
  flags = 0;
  i = -1;

  if (elf->e_type == ET_EXEC)
    flags |= EXEC_P;
  if (elf->e_type == ET_DYN)
    flags |= DYNAMIC;
  if (elf->e_phnum > 0)
    flags |= D_PAGED;

  while (++i < elf->e_shnum)
    {
      sec = buf + elf->e_shoff + (elf->e_shentsize * i);
      if (sec->sh_type == SHT_SYMTAB)
	flags |= HAS_SYMS;
      if (sec->sh_type == SHT_REL || sec->sh_type == SHT_RELA)
	flags |= HAS_RELOC;
    }
  return (flags);
}
