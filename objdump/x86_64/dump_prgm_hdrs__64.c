/*
** dump_prgm_hdrs__64.c for my_objdump in /home/poussi_w//projects/nm-objdump/objdump/x86_64
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb  1 12:35:38 2012 william poussier
** Last update Tue Feb 21 09:30:52 2012 william poussier
*/

#include <elf.h>
#include <stdio.h>
#include "my_objdump.h"

int		dump_prgm_hdrs__64(void *buf)
{
  Elf64_Ehdr	*elf;
  Elf64_Phdr	*phdr;
  char		*pt;
  int		i;

  i = -1;
  elf = buf;

  if (elf->e_phnum != 0)
    printf("Program Header:\n");

  while (++i < elf->e_phnum)
    {
      phdr = buf + elf->e_phoff + (elf->e_phentsize * i);
      pt = objdump_parse_segment_type(phdr->p_type);
      dump_prgm_hdr__64(phdr, pt);
    }
  dump_dynamic_scn__64(buf, elf);
  dump_verdef_scn__64(buf, elf);
  dump_verneed_scn__64(buf, elf);

  if (elf->e_phnum != 0)
    printf("\n");

  return (0);
}
