/*
** get_scn_hdr_flags__64.c for my_objdump in /home/poussi_w//projects/nm-objdump
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb  1 00:21:02 2012 william poussier
** Last update Tue Feb 21 09:42:01 2012 william poussier
*/

#include "my_objdump.h"

void		get_scn_hdr_flags_next__64(Elf64_Shdr *scn, unsigned int flags)
{
  if ((scn->sh_flags & SHF_MERGE) != 0)
    {
      flags |= SEC_MERGE;
      if ((scn->sh_flags & SHF_STRINGS) != 0)
	flags |= SEC_STRINGS;
    }
  if ((scn->sh_flags & SHF_TLS) != 0)
    flags |= SEC_THREAD_LOCAL;
  if ((scn->sh_flags & SHF_EXCLUDE) != 0)
    flags |= SEC_EXCLUDE;
}

unsigned int	get_scn_hdr_flags__64(Elf64_Shdr *scn)
{
  unsigned int	flags;

  flags = SEC_NO_FLAGS;

  if (scn->sh_type != SHT_NOBITS)
    flags |= SEC_HAS_CONTENTS;
  if (scn->sh_type == SHT_GROUP)
    flags |= SEC_GROUP | SEC_EXCLUDE;
  if ((scn->sh_flags & SHF_ALLOC) != 0)
    {
      flags |= SEC_ALLOC;
      if (scn->sh_type != SHT_NOBITS)
	flags |= SEC_LOAD;
    }
  if ((scn->sh_flags & SHF_WRITE) == 0)
    flags |= SEC_READONLY;
  if ((scn->sh_flags & SHF_EXECINSTR) != 0)
    flags |= SEC_CODE;
  else if ((flags & SEC_LOAD) != 0)
    flags |= SEC_DATA;

  get_scn_hdr_flags_next__64(scn, flags);

  return (flags);
}
