/*
** dump_verdef_scn__64.c for my_objdump in /home/poussi_w//projects/nm-objdump
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Fri Feb  3 04:47:57 2012 william poussier
** Last update Tue Feb 21 09:54:02 2012 william poussier
*/

#include <elf.h>
#include <stdio.h>
#include <stdlib.h>
#include "my_objdump.h"

void		dump_verdef_scn__64(void *buf, Elf64_Ehdr *elf)
{
  Elf64_Shdr	*scn;
  Elf64_Shdr	*dynscn;
  Elf64_Verdef	*verd;
  Elf64_Verdaux	*verda;
  char		*dynstr;
  int		i;

  i = 0;
  dynstr = NULL;
  verd = NULL;
  verda = NULL;

  if ((scn = elf_getscn_by_type__64(buf, SHT_GNU_verdef)) != NULL)
    verd = buf + scn->sh_offset;
  if ((scn = elf_getscn_by_type__64(buf, SHT_DYNSYM)) != NULL)
    {
      dynscn = buf + elf->e_shoff + (scn->sh_link * sizeof(Elf64_Shdr));
      dynstr = buf + dynscn->sh_offset;
    }

  if (verd != NULL && dynstr != NULL)
    {
      printf("\nVersion definitions:\n");
      aff_verdef_scn__64(verd, dynstr, verda, i);
    }
}

void		aff_verdef_scn__64(Elf64_Verdef *verd, char *dynstr,
				   Elf64_Verdaux *verda, int i)
{
  int		cont;

  do
    {
      verda = (Elf64_Verdaux *)((char *) verd + verd->vd_aux);

      printf(DMP_VERDEX, verd->vd_ndx, verd->vd_flags, verd->vd_hash,
	     verda->vda_name ? (dynstr + verda->vda_name) : CORRUPT);
      i = 1;

      if (verd->vd_aux != 0 && verda->vda_next != 0)
	{
	  printf("\t");
	  while (i < verd->vd_cnt)
	    {
	      verda = (Elf64_Verdaux *)((char *) verda + verda->vda_next);
	      printf ("%s ", verda->vda_name ? (dynstr + verda->vda_name) : CR);
	      i++;
	    }
	  printf("\n");
	}
      cont = verd->vd_next != 0;
      verd = (Elf64_Verdef *)((char *) verd + verd->vd_next);
    }
  while (cont);
}
