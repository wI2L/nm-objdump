/*
** my_nm.c for my_nm in /home/poussi_w//projects/nm-objdump
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Tue Dec 27 16:53:49 2011 william poussier
** Last update Sat Dec 31 08:19:08 2011 william poussier
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "my_nm.h"

int		my_nm(int ac, char **av)
{
  int		i;
  t_nm_options	*op;
  char		**files;

  i = 0;
  op = nm_get_options();
  files = nm_parse_options(ac, av, op);

  if (!files)
    nm_parse_file(FILE_DEFAULT, op);
  else
    {
      while (files[i] != NULL)
	{
	  if (my_tablen(files) >= 2)
	    printf(PRINT_FILENAME, files[i]);
	  nm_parse_file(files[i], op);
	  i++;
	}
      free(files);
    }
  return (0);
}

int	main(int ac, char **argv)
{
  get_progname(parse_progname(argv[0]));

  if (ac == 2)
    {
      if (strcmp(argv[1], "--help") == 0)
	nm_usage();
    }
  my_nm(ac, argv);

  return (0);
}
