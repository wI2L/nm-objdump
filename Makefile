##
## Makefile for my_objdump in /home/poussi_w//projects/nm-objdump
##
## Made by william poussier
## Login   <poussi_w@epitech.net>
##
## Started on  Thu Dec 22 22:45:22 2011 william poussier
## Last update Fri Mar  9 07:28:12 2012 william poussier
##

NAME_OBJDUMP	=	my_objdump

NAME_NM		=	my_nm

SRC_OBJDUMP	=       ./my_objdump.c					\
			./parse/objdump_parse_file.c			\
			./parse/objdump_parse_type.c			\
			./parse/objdump_parse_options.c			\
			./parse/objdump_parse_isoption.c		\
			./parse/objdump_parse_flags.c                   \
			./parse/objdump_usage.c                         \
			./parse/objdump_parse_ar.c			\
			./parse/objdump_parse_segment_type.c		\
			./parse/objdump_parse_dyn_segment_type.c	\
			./objdump/i386/dump_scn_content__32.c		\
			./objdump/i386/dump_header__32.c		\
			./objdump/i386/dump_scn_content_rules__32.c	\
			./objdump/i386/dump_scn_content_split__32.c	\
			./objdump/i386/dump_scn_content_hexa__32.c	\
			./objdump/i386/dump_scn_content_char__32.c	\
			./objdump/i386/dump_content_off__32.c		\
			./objdump/i386/get_file_hdr_flags__32.c		\
			./objdump/i386/dump_scn_hdrs__32.c		\
			./objdump/i386/get_scn_hdr_flags__32.c		\
			./objdump/i386/dump_scn_hdr__32.c		\
			./objdump/i386/iterate_over_scns__32.c		\
			./objdump/i386/dump_prgm_hdrs__32.c		\
			./objdump/i386/dump_prgm_hdr__32.c		\
			./objdump/i386/dump_dynamic_scn__32.c		\
			./objdump/i386/dump_verneed_scn__32.c		\
			./objdump/i386/dump_verdef_scn__32.c		\
			./objdump/i386/dump_scn_hdr_rules__32.c		\
			./objdump/x86_64/dump_scn_content__64.c		\
			./objdump/x86_64/dump_header__64.c		\
			./objdump/x86_64/dump_scn_content_rules__64.c	\
			./objdump/x86_64/dump_scn_content_split__64.c	\
			./objdump/x86_64/dump_scn_content_hexa__64.c	\
			./objdump/x86_64/dump_scn_content_char__64.c	\
			./objdump/x86_64/dump_content_off__64.c		\
			./objdump/x86_64/get_file_hdr_flags__64.c	\
			./objdump/x86_64/dump_scn_hdrs__64.c		\
			./objdump/x86_64/get_scn_hdr_flags__64.c	\
			./objdump/x86_64/dump_scn_hdr__64.c		\
			./objdump/x86_64/iterate_over_scns__64.c	\
			./objdump/x86_64/dump_prgm_hdrs__64.c		\
			./objdump/x86_64/dump_prgm_hdr__64.c		\
			./objdump/x86_64/dump_dynamic_scn__64.c		\
			./objdump/x86_64/dump_verneed_scn__64.c		\
			./objdump/x86_64/dump_verdef_scn__64.c		\
			./objdump/x86_64/dump_scn_hdr_rules__64.c

SRC_NM		=	./my_nm.c					\
			./parse/nm_parse_file.c				\
			./parse/nm_parse_type.c				\
			./parse/nm_parse_ar.c				\
			./parse/nm_usage.c				\
			./parse/nm_parse_options.c			\
			./parse/nm_parse_isoption.c			\
			./parse/nm_get_options.c			\
			./nm/i386/dump_print__32.c			\
			./nm/i386/dump_search__32.c			\
			./nm/i386/dump_symbols__32.c			\
			./nm/i386/get_symbtype__32.c			\
			./nm/i386/list_add__32.c			\
			./nm/i386/list_add_nosort__32.c			\
			./nm/i386/list_alphasort__32.c			\
			./nm/i386/list_numsort__32.c			\
			./nm/i386/list_print__32.c			\
			./nm/i386/list_free__32.c			\
			./nm/i386/list_rev__32.c			\
			./nm/i386/print_symbol__32.c			\
			./nm/i386/iterate_symtype_over_scns__32.c	\
			./nm/x86_64/dump_print__64.c			\
			./nm/x86_64/dump_search__64.c			\
			./nm/x86_64/dump_symbols__64.c			\
			./nm/x86_64/get_symbtype__64.c			\
			./nm/x86_64/list_add__64.c			\
			./nm/x86_64/list_add_nosort__64.c		\
			./nm/x86_64/list_alphasort__64.c		\
			./nm/x86_64/list_numsort__64.c			\
			./nm/x86_64/list_print__64.c			\
			./nm/x86_64/list_free__64.c			\
			./nm/x86_64/list_rev__64.c			\
			./nm/x86_64/print_symbol__64.c			\
			./nm/x86_64/iterate_symtype_over_scns__64.c


SRC_UTILS	=	./utils/my/parse_progname.c			\
			./utils/my/get_progname.c			\
			./utils/my/my_tablen.c				\
			./utils/my/my_strmode.c				\
			./utils/my/get_exp_pow2.c			\
			./utils/my/my_node_swap__64.c			\
			./utils/my/my_node_swap__32.c			\
			./utils/elf/elf_check.c				\
			./utils/elf/elf_getclass.c			\
			./utils/elf/elf_gettarget.c			\
			./utils/elf/elf_getarchi.c			\
			./utils/elf/elf_getscn_by_type__32.c		\
			./utils/elf/elf_getscn_by_name__32.c		\
			./utils/elf/elf_getshstrtab__32.c		\
			./utils/elf/elf_getscn_by_type__64.c		\
			./utils/elf/elf_getscn_by_name__64.c		\
			./utils/elf/elf_getshstrtab__64.c		\
			./utils/elf/elf_getstrtab__32.c			\
			./utils/elf/elf_getstrtab__64.c			\
			./utils/ar/ar_check.c				\
			./utils/ar/ar_getmember.c			\
			./utils/ar/ar_getmember_wh.c			\
			./utils/ar/ar_check_member.c			\
			./utils/ar/ar_getmember_data.c			\
			./utils/ar/ar_getmember_data_wh.c		\
			./utils/ar/ar_getmember_name.c			\
			./utils/ar/ar_parse_data.c			\
			./utils/ar/ar_extract_hdata.c			\
			./utils/ar/ar_aff_hdata.c

SRC_XLIB	=	./xlib/xopen.c					\
			./xlib/xclose.c					\
			./xlib/xmmap.c					\
			./xlib/xfstat.c					\
			./xlib/xmunmap.c				\
			./xlib/xmalloc.c				\
			./xlib/xstrftime.c


OBJ_OBJDUMP	= 	$(SRC_OBJDUMP:.c=.o)

OBJ_NM		=	$(SRC_NM:.c=.o)

OBJ_UTILS	=	$(SRC_UTILS:.c=.o)

OBJ_XLIB	=	$(SRC_XLIB:.c=.o)


CC              =       @gcc

STRIP		=	@strip

RM		= 	@rm -f

CFLAGS		= 	-Wall -W -Werror -Wno-uninitialized -O3 -I./include

LFLAGS		=

all		:	$(NAME_OBJDUMP) $(NAME_NM)

objdump		:	$(NAME_OBJDUMP)

nm		:	$(NAME_NM)

.c.o		:	$(CC) $(CFLAGS) $(INCLUDES) $(LFLAGS) -c $< -o $(<:.c=.o)

$(NAME_OBJDUMP)	:	$(OBJ_OBJDUMP) $(OBJ_UTILS) $(OBJ_XLIB)
			$(CC) -o $(NAME_OBJDUMP) $(OBJ_OBJDUMP) $(OBJ_UTILS) $(OBJ_XLIB) $(LFLAGS)
##			$(STRIP) $(NAME_OBJDUMP)

$(NAME_NM)	:	$(OBJ_NM) $(OBJ_UTILS) $(OBJ_XLIB)
			$(CC) -o $(NAME_NM) $(OBJ_NM) $(OBJ_UTILS) $(OBJ_XLIB) $(LFLAGS)
##			$(STRIP) $(NAME_NM)

clean		:
			@find . \( -name "*.o" -o -name "*~" -o -name "#*#" \) -exec rm {} \;
##			$(RM) $(OBJ)
##			$(RM) *~

fclean		:	clean
			$(RM) $(NAME_OBJDUMP)
			$(RM) $(NAME_NM)

re		: 	fclean all

.PHONY		:	all re clean fclean

debug		:	CFLAGS += -g
debug		:	fclean all