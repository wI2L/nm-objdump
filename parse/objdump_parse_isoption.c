/*
** objdump_parse_isoption.c for my_objdump in /home/poussi_w//projects/nm-objdump/parse
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Fri Dec 23 06:00:44 2011 william poussier
** Last update Mon Feb 20 20:04:37 2012 william poussier
*/

#include <getopt.h>
#include "my_objdump.h"

void	objdump_set_options(int opt, t_objdump_options *op)
{
  if (opt == 'a')
    op->a = 1;
  if (opt == 'f')
    op->f = 1;
  if (opt == 'F')
    op->F = 1;
  if (opt == 'h')
    op->h = 1;
  if (opt == 'p')
    op->p = 1;
  if (opt == 'r')
    op->r = 1;
  if (opt == 'R')
    op->R = 1;
  if (opt == 's')
    op->s = 1;
}

int	objdump_parse_isoption(int ac, char **av, t_objdump_options *op)
{
  int	opt;

  while ((opt = getopt(ac, av, "afFhprRs")) != -1)
    {
      objdump_set_options(opt, op);

      if (opt == '?')
	objdump_usage();
    }
  if (op->f == 0 && op->s == 0
      && op->a == 0
      && op->h == 0
      && op->p == 0
      && op->r == 0)
    {
      op->f = 1;
      op->s = 1;
    }
  return (optind);
}

