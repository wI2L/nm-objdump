/*
** objdump_parse_flags.c for my_objdump in /home/poussi_w//projects/nm-objdump/parse
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Sun Dec 25 08:28:07 2011 william poussier
** Last update Thu Mar  1 19:00:51 2012 william poussier
*/

#include <stdio.h>
#include <unistd.h>
#include "my_objdump.h"

void            objdump_parse_file_flags(int flags)
{
  char		*comma = "";

#define PF(x, y)	if (flags & x) { printf(FLAG, comma, y); comma = ", "; }

  PF (HAS_RELOC, "HAS_RELOC");
  PF (EXEC_P, "EXEC_P");
  PF (HAS_SYMS, "HAS_SYMS");
  PF (DYNAMIC, "DYNAMIC");
  PF (D_PAGED, "D_PAGED");

  printf("\n");

#undef PF
}

void		objdump_parse_scn_flags(unsigned int flags)
{
  char		*comma = "";

#define	PF(x, y)	if (flags & x) { printf(FLAG, comma, y); comma = ", "; }

  PF (SEC_HAS_CONTENTS, "CONTENTS");
  PF (SEC_ALLOC, "ALLOC");
  PF (SEC_CONSTRUCTOR, "CONSTRUCTOR");
  PF (SEC_LOAD, "LOAD");
  PF (SEC_RELOC, "RELOC");
  PF (SEC_READONLY, "READONLY");
  PF (SEC_CODE, "CODE");
  PF (SEC_DATA, "DATA");
  PF (SEC_ROM, "ROM");
  PF (SEC_DEBUGGING, "DEBUGGING");
  PF (SEC_NEVER_LOAD, "NEVER_LOAD");
  PF (SEC_EXCLUDE, "EXCLUDE");
  PF (SEC_SORT_ENTRIES, "SORT_ENTRIES");
  PF (SEC_THREAD_LOCAL, "THREAD_LOCAL");
  PF (SEC_GROUP, "GROUP");

  printf("\n");

#undef PF
}
