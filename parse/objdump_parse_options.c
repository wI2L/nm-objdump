/*
** objdump_parse_options.c for my_objdump in /home/poussi_w//projects/nm-objdump/parse
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Fri Dec 23 05:45:42 2011 william poussier
** Last update Sat Dec 31 03:50:03 2011 william poussier
*/

#include <stdlib.h>
#include <getopt.h>
#include "my_objdump.h"

char	**objdump_parse_options(int argc, char **av, t_objdump_options *op)
{
  char	**files;
  int	optind;
  int	i;

  i = 0;
  files = NULL;
  optind = objdump_parse_isoption(argc, av, op);

  if (optind < argc)
    {
      while (optind < argc)
	{
	  files = realloc(files, (sizeof(char *) * (i + 1)));
	  files[i] = av[optind++];
	  i = i + 1;
	}
      files = realloc(files, (sizeof(char *) * (i + 1)));
      files[i] = NULL;

      return (files);
    }
  return (NULL);
}
