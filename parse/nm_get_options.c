/*
** nm_get_options.c for my_nm in /home/poussi_w//projects/nm-objdump/parse
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Sat Dec 31 04:05:48 2011 william poussier
** Last update Sat Dec 31 04:24:34 2011 william poussier
*/

#include <time.h>
#include <string.h>
#include <stdlib.h>
#include "my_nm.h"

t_nm_options		*nm_get_options(void)
{
  static int		_is_set = 0;
  static t_nm_options	op;

  if (_is_set == 0)
    {
      memset(&op, 0, sizeof(op));
      _is_set = 1;
    }
  return (&op);
}
