/*
** objdump_parse_segment_type.c for my_objdump in /home/poussi_w//projects/nm-objdump
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb  1 10:07:58 2012 william poussier
** Last update Mon Feb 20 20:02:28 2012 william poussier
*/

#include <elf.h>
#include "my_objdump.h"

char	*objdump_parse_segment_type(unsigned int p_type)
{
  if (p_type == PT_TLS)
    return ("TLS");
  if (p_type == PT_NULL)
    return ("NULL");
  if (p_type == PT_LOAD)
    return ("LOAD");
  if (p_type == PT_NOTE)
    return ("NOTE");
  if (p_type == PT_PHDR)
    return ("PHDR");
  if (p_type == PT_SHLIB)
    return ("SHLIB");
  if (p_type == PT_INTERP)
    return ("INTERP");
  if (p_type == PT_DYNAMIC)
    return ("DYNAMIC");
  if (p_type == PT_GNU_STACK)
    return ("STACK");
  if (p_type == PT_GNU_RELRO)
    return ("RELRO");
  if (p_type == PT_GNU_EH_FRAME)
    return ("EH_FRAME");

  return (NULL);
}
