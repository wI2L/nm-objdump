/*
** nm_usage.c for my_nm in /home/poussi_w//projects/nm-objdump/parse
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Tue Dec 27 17:03:15 2011 william poussier
** Last update Mon Jan  2 05:00:54 2012 william poussier
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "my_nm.h"

void	nm_usage(void)
{
  char	*progname;

  progname = get_progname(NULL);

  fprintf(stderr,
	  "Usage: %s <options> <files>\n\n"
	  " List symbols in <files> (a.out by default)\n\n"
	  "  -A\tPrint name of the input file before every symbol\n"
	  "  -d\tDisplay only defined symbols\n"
	  "  -D\tDisplay dynamic symbols instead of normal symbols\n"
	  "  -n\tSort symbols numerically by address\n"
	  "  -p\tDo not sort the symbols\n"
	  "  -r\tReverse the sense of the sort\n"
	  "  -S\tPrint size of defined symbols\n"
	  "  -u\tDisplay only undefined symbols\n"
          "\n%s: supported targets: elf64-x86-64 elf32-i386"
	  "\n%s: supported architectures: i386 i386:x86-64"
	  "\n%s: supported files: dynamic relocatable executable ar\n",
	  progname, progname, progname, progname);

  exit(EXIT_SUCCESS);
}
