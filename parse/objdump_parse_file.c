/*
** objdump_parse_file.c for my_objdump in /home/poussi_w//projects/nm-objdump/parse
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Dec 22 22:18:28 2011 william poussier
** Last update Sun Jan 29 16:42:29 2012 william poussier
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <elf.h>
#include "my_objdump.h"
#include "xlib.h"

int		objdump_parse_file(char *file, t_objdump_options *op)
{
  struct stat	ss;
  void		*buf;
  int		fd;

  fd = xopen(file, O_RDONLY, file);

  xfstat(fd, &ss, file);

  buf = xmmap(NULL, ss.st_size, PROT_READ, MAP_PRIVATE, fd, 0);

  if (ar_check(buf))
    objdump_parse_ar(buf, ss.st_size, op, file);
  else
    {
      if (elf_check(buf))
	objdump_parse_type(buf, file, NULL, op);
      else
	fprintf(stderr, BAD_FORMAT, get_progname(NULL), file);
  }

  xclose(fd);
  xmunmap(buf, ss.st_size);

  return (0);
}
