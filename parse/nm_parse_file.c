/*
** nm_parse_file.c for my_nm in /home/poussi_w//projects/nm-objdump/parse
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Tue Dec 27 17:21:18 2011 william poussier
** Last update Mon Jan  2 23:55:12 2012 william poussier
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <elf.h>
#include "my_nm.h"
#include "xlib.h"

int		nm_parse_file(char *file, t_nm_options *op)
{
  struct stat	s;
  int		fd;
  void		*buf;

  fd = xopen(file, O_RDONLY, file);

  xfstat(fd, &s, file);

  buf = xmmap(NULL, s.st_size, PROT_READ, MAP_PRIVATE, fd, 0);

  if (ar_check(buf))
    nm_parse_ar(buf, s.st_size, file, op);
  else
    {
      if (elf_check(buf))
	nm_parse_type(buf, file);
      else
        fprintf(stderr, BAD_FORMAT, get_progname(NULL), file);
    }

  xmunmap(buf, s.st_size);
  xclose(fd);

  return (0);
}
