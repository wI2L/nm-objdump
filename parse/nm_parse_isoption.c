/*
** nm_parse_isoption.c for my_nm in /home/poussi_w//projects/nm-objdump/parse
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Sat Dec 31 04:13:25 2011 william poussier
** Last update Mon Jan  2 04:13:19 2012 william poussier
*/

#include <getopt.h>
#include "my_nm.h"

int	nm_parse_isoption(int ac, char **av, t_nm_options *op)
{
  int	opt;

  while ((opt = getopt(ac, av, "AdDnprSu")) != -1)
    {
      if (opt == 'A')
	op->A = 1;
      if (opt == 'd')
	op->d = 1;
      if (opt == 'D')
	op->D = 1;
      if (opt == 'n')
	op->n = 1;
      if (opt == 'p')
	op->p = 1;
      if (opt == 'r')
	op->r = 1;
      if (opt == 'S')
	op->S = 1;
      if (opt == 'u')
	op->u = 1;
      if (opt == '?')
	nm_usage();
    }
  return (optind);
}

