/*
** nm_parse_ar.c for my_nm in /home/poussi_w//projects/nm-objdump/parse
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Dec 29 00:38:19 2011 william poussier
** Last update Wed Jan 18 11:06:45 2012 william poussier
*/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "my_nm.h"
#include "xlib.h"

void		nm_parse_ar(void *buf, size_t fsize,
			    char *file, t_nm_options *op)
{
  static char	member[AR_BUFFER];
  void		*data;
  char		*name;

  name = xmalloc(AR_BUFFER * sizeof(char));
  ar_getmember(NULL, NULL, 0, NULL, 1);

  while ((data = ar_getmember(file, buf, fsize, member, 0)) != NULL)
    {
      if (strcmp(data, AR_NODATA) != 0)
	{
	  if (elf_check(data))
	    {
	      if (!op->A)
		printf(AR_MEMBER_NAME, member);
	      strcpy(name, file);
	      strcat(name, ":");
	      strcat(name, member);
	      nm_parse_type(data, name);
	    }
	  else
	    fprintf(stderr, BAD_FORMAT, get_progname(NULL), name);
	}
    }
  free(name);
}
