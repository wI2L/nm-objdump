/*
** objdump_parse_ar.c for my_objdump in /home/poussi_w//projects/nm-objdump/parse
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Dec 29 04:55:19 2011 william poussier
** Last update Sun Jan 29 16:47:18 2012 william poussier
*/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "my_objdump.h"
#include "xlib.h"

void		objdump_parse_ar(void *buf, size_t fsize,
				 t_objdump_options *op, char *ar_file)
{
  void		*data;
  t_v_ar	h_ar;

  h_ar.name = xmalloc(AR_BUFFER * sizeof(char));

  printf(AR_DECL, ar_file);

  ar_getmember_wh(NULL, NULL, 0, NULL, 1);

  while ((data = ar_getmember_wh(ar_file, buf, fsize, &h_ar, 0)) != NULL)
    {
      if (strcmp(data, AR_NODATA) != 0)
	{
	  if (elf_check(data))
	    objdump_parse_type(data, h_ar.name, &h_ar, op);
	  else
	    fprintf(stderr, BAD_FORMAT, get_progname(NULL), h_ar.name);
	}
    }
  free(h_ar.name);
}
