/*
** nm_parse_type.c for my_nm in /home/poussi_w//projects/nm-objdump/parse
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Tue Dec 27 17:21:46 2011 william poussier
** Last update Thu Feb 23 00:19:35 2012 william poussier
*/

#include <elf.h>
#include "my_nm.h"
#include "syms.h"

int		nm_parse_type(void *buf, char *file)
{
  Elf_Ehdr	*elf;

  elf = buf;

  if (elf->e_ident[EI_CLASS] == ELFCLASS32)
    {
      dump_symbols__32(buf, file);
    }
  if (elf->e_ident[EI_CLASS] == ELFCLASS64)
    {
      dump_symbols__64(buf, file);
    }
  return (0);
}
