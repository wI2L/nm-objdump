/*
** objdump_parse_type.c for my_objdump in /home/poussi_w//projects/nm-objdump/parse
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Fri Dec 23 06:07:46 2011 william poussier
** Last update Wed Feb 22 18:03:00 2012 william poussier
*/

#include <elf.h>
#include "my_objdump.h"

int		objdump_parse_type(void *buf, char *file, t_v_ar *h_ar,
				   t_objdump_options *op)
{
  Elf_Ehdr	*elf;

  elf = buf;

  if (elf->e_ident[EI_CLASS] == ELFCLASS32)
    {
      dump_header__32(buf, file, h_ar, op);
      if (op->p && elf->e_shoff)
	dump_prgm_hdrs__32(buf);
      if (op->h)
	dump_scn_hdrs__32(buf);
      if (op->s && elf->e_shoff)
	dump_scn_content__32(buf, op);
    }
  if (elf->e_ident[EI_CLASS] == ELFCLASS64)
    {
      dump_header__64(buf, file, h_ar, op);
      if (op->p && elf->e_shoff)
	dump_prgm_hdrs__64(buf);
      if (op->h)
	dump_scn_hdrs__64(buf);
      if (op->s && elf->e_shoff)
	dump_scn_content__64(buf, op);
    }
  return (0);
}
