/*
** usage.c for my_objdump in /home/poussi_w//projects/nm-objdump/parse
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Fri Dec 23 06:14:14 2011 william poussier
** Last update Wed Feb 29 22:58:29 2012 william poussier
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "my_objdump.h"

void	objdump_usage(void)
{
  char	*progname;

  progname = get_progname(NULL);

  fprintf(stderr,
	  "Usage: %s <options> <files>\n\n"
	  " Display informations from object <files> (a.out by default).\n"
	  " If no options are specified, "
	  "then the program will run with options -f -s\n\n"
	  "  -a\tDisplay archive header information\n"
	  "  -f\tDisplay the content of the overall file header\n"
	  "  -h\tDisplay the contents of the section headers\n"
	  "  -p\tDisplay object format specific file header contents\n"
	  "  -s\tDisplay the full contents of all sections\n"
	  "  -F\tInclude file offsets when displaying information\n"
	  "\n%s: supported targets: elf64-x86-64 elf32-i386"
	  "\n%s: supported architectures: i386 i386:x86-64"
	  "\n%s: supported files: dynamic relocatable executable ar\n",
	  progname, progname, progname, progname);

  exit(EXIT_SUCCESS);
}
