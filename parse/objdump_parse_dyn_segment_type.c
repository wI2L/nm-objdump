/*
** objdump_parse_dyn_segment_type.c for my_objdump in /home/poussi_w//projects/nm-objdump/parse
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb  2 02:27:20 2012 william poussier
** Last update Tue Feb 21 17:37:46 2012 william poussier
*/

#include <stdlib.h>
#include "my_objdump.h"
#include "dynamic.h"

extern	t_dyn_type	dynamic_type[];

char	*objdump_parse_dyn_segment_type(unsigned int d_tag, int *stringp)
{
  char	*pt;
  int	i;

  i = 0;
  pt = NULL;

  while (dynamic_type[i].dt_name != NULL)
    {
      if (dynamic_type[i].dt_type == d_tag)
	pt = dynamic_type[i].dt_name;
      i++;
    }

  if (d_tag == DT_NEEDED
      || d_tag == DT_SONAME
      || d_tag == DT_RPATH
      || d_tag == DT_RUNPATH
      || d_tag == DT_CONFIG
      || d_tag == DT_DEPAUDIT
      || d_tag == DT_FILTER)
    {
      *stringp = 1;
    }

  return (pt);
}
