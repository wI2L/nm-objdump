/*
** nm_parse_options.c for my_nm in /home/poussi_w//projects/nm-objdump/parse
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Sat Dec 31 04:12:08 2011 william poussier
** Last update Sat Dec 31 04:12:50 2011 william poussier
*/

#include <stdlib.h>
#include <getopt.h>
#include "my_nm.h"

char	**nm_parse_options(int argc, char **av, t_nm_options *op)
{
  char	**files;
  int	optind;
  int	i;

  i = 0;
  files = NULL;
  optind = nm_parse_isoption(argc, av, op);

  if (optind < argc)
    {
      while (optind < argc)
	{
	  files = realloc(files, (sizeof(char *) * (i + 1)));
	  files[i] = av[optind++];
	  i = i + 1;
	}
      files = realloc(files, (sizeof(char *) * (i + 1)));
      files[i] = NULL;

      return (files);
    }
  return (NULL);
}
