/*
** my_objdump.c for my_objdump in /home/poussi_w//projects/nm-objdump
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Dec 22 20:58:05 2011 william poussier
** Last update Tue Jan  3 03:16:25 2012 william poussier
*/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "my_objdump.h"

int			my_objdump(int ac, char **argv)
{
  int			i;
  t_objdump_options	op;
  char			**files;

  i = 0;
  memset(&op, 0, sizeof(op));
  files = objdump_parse_options(ac, argv, &op);

  if (!files)
    objdump_parse_file(FILE_DEFAULT, &op);
  else
    {
      while (files[i] != NULL)
	{
	  objdump_parse_file(files[i], &op);
	  i++;
	}
      free(files);
    }
  return (0);
}

int	main(int ac, char **argv)
{
  get_progname(parse_progname(argv[0]));

  if (ac == 2)
    {
      if (strcmp(argv[1], "--help") == 0)
	objdump_usage();
    }
  my_objdump(ac, argv);

  return (0);
}
