/*
** dump_print__64.c for my_nm in /home/poussi_w//projects/nm-objdump/nm/x86_64
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb 22 22:50:32 2012 william poussier
** Last update Wed Feb 22 22:50:50 2012 william poussier
*/

#include <elf.h>
#include <stdlib.h>
#include "my_nm.h"

void		dump_print__64(void *buf, int nb_syms,
			       Elf64_Shdr *symscn, Elf64_Shdr *strscn)
{
  int		i;
  char		*strtab;
  t_list_64	*list;
  Elf64_Sym	*sym;

  i = 0;
  list = NULL;

  sym = buf + symscn->sh_offset;
  strtab = buf + strscn->sh_offset;

  if (sym != NULL)
    {
      while (i < nb_syms)
	{
	  if (sym[i].st_info != 4 && strtab[sym[i].st_name] != 0)
	    list_add__64(&sym[i], strtab + sym[i].st_name, &list);
	  i++;
	}
      list_print__64(buf, &list);
      list_free__64(&list);
    }
}
