/*
** list_print__64.c for my_nm in /home/poussi_w//projects/nm-objdump/nm/x86_64
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb 22 22:55:47 2012 william poussier
** Last update Sat Mar 10 02:21:13 2012 william poussier
*/

#include <elf.h>
#include <stdio.h>
#include <stdlib.h>
#include "my_nm.h"

void		list_print__64(void *buf, t_list_64 **begin)
{
  t_nm_options	*op;
  t_list_64	*tmp;

  op = nm_get_options();

  if (op->n && !op->p)
    {
      list_alphasort__64(begin);
      list_numsort__64(begin);
    }
  else if (!op->p)
    {
      list_numsort__64(begin);
      list_alphasort__64(begin);
    }
  if (op->r && !op->p)
    list_rev__64(begin);

  tmp = *begin;

  while (tmp != NULL)
    {
      print_symbol__64(buf, tmp, op);
      tmp = tmp->next;
    }
}
