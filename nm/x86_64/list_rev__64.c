/*
** list_rev__64.c for my_nm in /home/poussi_w//projects/nm-objdump/nm/x86_64
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb 22 22:56:18 2012 william poussier
** Last update Wed Feb 22 22:56:28 2012 william poussier
*/

#include <stdlib.h>
#include "my_nm.h"

void		list_rev__64(t_list_64 **begin)
{
  t_list_64	*tmp;
  t_list_64	*prev;

  tmp = NULL;
  prev = NULL;

  if (begin != NULL)
    {
      while (*begin != NULL)
	{
	  tmp = *begin;
	  *begin = tmp->next;
	  tmp->next = prev;
	  prev = tmp;
	}
      *begin = tmp;
    }
}
