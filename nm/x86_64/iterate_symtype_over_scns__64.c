/*
** iterate_symtype_over_scns__64.c for my_nm in /home/poussi_w//work/projects/c/nmobjdump-2015-2014s-poussi_w
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Fri Mar  9 05:03:18 2012 william poussier
** Last update Fri Mar  9 05:03:26 2012 william poussier
*/

#include <elf.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include "my_nm.h"

extern t_syms_type	symst_db[];

char			iterate_symtype_over_scns__64(char *str)
{
  int			i;

  i = -1;

  while (++i < 19)
    {
      if (strncmp(str, symst_db[i].scn, strlen(symst_db[i].scn)) == 0)
	return (symst_db[i].type);

    }
  return ('?');
}
