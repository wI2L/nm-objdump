/*
** list_add__64.c for my_nm in /home/poussi_w//projects/nm-objdump/nm/x86_64
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb 22 22:52:51 2012 william poussier
** Last update Fri Mar  9 05:44:45 2012 william poussier
*/

#include <elf.h>
#include <string.h>
#include <stdlib.h>
#include "my_nm.h"
#include "xlib.h"

void		list_add__64(Elf64_Sym *sym, char *str, t_list_64 **begin)
{
  t_list_64	*elem;
  t_nm_options	*op;

  op = nm_get_options();

  elem = xmalloc(sizeof(*elem));
  elem->sym = sym;
  elem->str = str;
  elem->next = NULL;

  list_add_nosort__64(elem, begin);
}
