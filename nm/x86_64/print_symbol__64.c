/*
** print_symbol__64.c for my_nm in /home/poussi_w//projects/nm-objdump/nm/x86_64
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb 22 22:56:46 2012 william poussier
** Last update Wed Feb 22 22:56:59 2012 william poussier
*/

#include <stdio.h>
#include "my_nm.h"

void	print_symbol__64(void *buf, t_list_64 *sym, t_nm_options *op)
{
  if (sym->sym->st_shndx == SHN_UNDEF && !op->d)
    {
      if (op->A)
	printf(AR_FILENAME, op->filename);

      printf(SYM64_UNDEF, get_symbtype__64(buf, sym->sym), sym->str);
    }
  else if (!op->u && sym->sym->st_shndx != SHN_UNDEF)
    {
      if (op->A)
	printf(AR_FILENAME, op->filename);

      if (op->S && sym->sym->st_size != 0)
	printf(SYM64_STD_SIZE, sym->sym->st_value, sym->sym->st_size,
	       get_symbtype__64(buf, sym->sym), sym->str);
      else
	printf(SYM64_STD_NOSIZE, sym->sym->st_value,
	       get_symbtype__64(buf, sym->sym), sym->str);
    }
}
