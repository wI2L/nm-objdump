/*
** dump_search__64.c for my_nm in /home/poussi_w//projects/nm-objdump/nm/x86_64
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb 22 22:51:13 2012 william poussier
** Last update Sat Mar 10 02:00:38 2012 william poussier
*/

#include <elf.h>
#include <stdio.h>
#include <stdlib.h>
#include "my_nm.h"
#include "utils.h"

int		dump_search__64(void *buf, Elf64_Shdr **sym, Elf64_Shdr **str)
{
  t_nm_options	*op;

  op = nm_get_options();

  if (op->D)
    {
      *sym = elf_getscn_by_name__64(buf, ".dynsym");
      *str = elf_getscn_by_name__64(buf, ".dynstr");
    }
  else
    {
      *sym = elf_getscn_by_type__64(buf, SHT_SYMTAB);
      *str = elf_getscn_by_name__64(buf, ".strtab");
    }

  if (*sym == NULL || *str == NULL)
    return (1);

  return (0);
}
