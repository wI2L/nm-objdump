/*
** dump_symbols__64.c for my_nm in /home/poussi_w//projects/nm-objdump/nm/x86_64
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb 22 22:51:45 2012 william poussier
** Last update Sat Mar 10 02:01:06 2012 william poussier
*/

#include <elf.h>
#include <stdio.h>
#include <stdlib.h>
#include "my_nm.h"

void		dump_symbols__64(void *buf, char *file)
{
  Elf64_Shdr	*symscn;
  Elf64_Shdr	*strscn;
  t_nm_options	*op;
  int		nb_syms;

  symscn = NULL;
  strscn = NULL;
  op = nm_get_options();

  nb_syms = 0;
  op->filename = file;

  if (dump_search__64(buf, &symscn, &strscn))
    {
      fprintf(stderr, NO_SYMBOLS, get_progname(NULL), file);
      exit(EXIT_FAILURE);
    }

  nb_syms = symscn->sh_size / sizeof(Elf64_Sym);

  dump_print__64(buf, nb_syms, symscn, strscn);
}
