/*
** list_free__64.c for my_nm in /home/poussi_w//projects/nm-objdump/nm/x86_64
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb 22 22:55:21 2012 william poussier
** Last update Wed Feb 22 22:55:30 2012 william poussier
*/

#include <elf.h>
#include <stdlib.h>
#include "my_nm.h"

void		list_free__64(t_list_64 **begin)
{
  t_list_64	*tmp;
  t_list_64	*cur;

  cur = *begin;

  while (cur != NULL)
    {
      tmp = cur;
      cur = cur->next;
      free(tmp);
    }
}
