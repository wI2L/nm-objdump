/*
** get_symbtype__64.c for my_nm in /home/poussi_w//work/projects/c/nmobjdump-2015-2014s-poussi_w
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Fri Mar  9 05:02:47 2012 william poussier
** Last update Fri Mar  9 05:04:12 2012 william poussier
*/

#include <elf.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include "my_nm.h"

char		decode_section_type__64(Elf64_Shdr *scn)
{
  if ((scn->sh_flags & SHF_EXECINSTR) != 0)
    return ('t');

  if ((scn->sh_flags & SHF_ALLOC) != 0
      && scn->sh_type != SHT_NOBITS)
    {
      if ((scn->sh_flags & SHF_WRITE) == 0)
	return ('r');
      else
	return ('d');
    }
  if (scn->sh_type == SHT_NOBITS)
    return ('b');

  if ((scn->sh_type != SHT_NOBITS) && ((scn->sh_flags & SHF_WRITE) == 0))
    return ('n');

  return ('?');
}

char		decode_symclass__64(void *buf, Elf64_Sym *sym)
{
  Elf64_Ehdr	*elf;
  Elf64_Shdr	*scn;
  char		*str;
  char		c;

  elf = buf;
  scn = buf + elf->e_shoff + (elf->e_shentsize * sym->st_shndx);
  str = elf_getshstrtab__64(buf) + scn->sh_name;

  if (ELF64_ST_BIND(sym->st_info) == STB_GNU_UNIQUE)
    return ('u');

  if (sym->st_shndx)
    {
      c = iterate_symtype_over_scns__64(str);
      if (c == '?')
	c = decode_section_type__64(scn);
    }
  else
    return ('?');

  if (ELF64_ST_BIND(sym->st_info) == STB_GLOBAL)
    c = toupper(c);

  return (c);
}

char		get_symbtype__64(void *buf, Elf64_Sym *sym)
{
  if (sym->st_shndx == SHN_ABS)
    return ((ELF64_ST_BIND(sym->st_info) == STB_GLOBAL) ? 'A' : 'a');

  if (sym->st_shndx == SHN_COMMON)
    return ('C');

  if (sym->st_shndx == SHN_UNDEF)
    {
      if (ELF64_ST_BIND(sym->st_info) == STB_WEAK)
	return ((ELF64_ST_TYPE(sym->st_info) == STT_OBJECT) ? 'v' : 'w');
      else
	return ('U');
    }
  if (ELF64_ST_TYPE(sym->st_info) == STT_GNU_IFUNC)
    return ('i');

  if (ELF64_ST_BIND(sym->st_info) == STB_WEAK)
    return ((ELF64_ST_TYPE(sym->st_info) == STT_OBJECT) ? 'V' : 'W');

  return (decode_symclass__64(buf, sym));
}
