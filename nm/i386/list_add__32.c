/*
** list_add__32.c for my_nm in /home/poussi_w//projects/nm-objdump/nm/i386
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb 22 22:42:52 2012 william poussier
** Last update Fri Mar  9 07:28:28 2012 william poussier
*/

#include <elf.h>
#include <stdlib.h>
#include "my_nm.h"
#include "xlib.h"

void		list_add__32(Elf32_Sym *sym, char *str, t_list_32 **begin)
{
  t_list_32	*elem;
  t_nm_options	*op;

  op = nm_get_options();

  elem = xmalloc(sizeof(*elem));
  elem->sym = sym;
  elem->str = str;
  elem->next = NULL;

  list_add_nosort__32(elem, begin);
}
