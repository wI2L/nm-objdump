/*
** list_rev__32.c for my_nm in /home/poussi_w//projects/nm-objdump/nm/i386
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb 22 22:44:18 2012 william poussier
** Last update Wed Feb 22 22:44:20 2012 william poussier
*/

#include <stdlib.h>
#include "my_nm.h"

void		list_rev__32(t_list_32 **begin)
{
  t_list_32	*tmp;
  t_list_32	*prev;

  tmp = NULL;
  prev = NULL;

  if (begin != NULL)
    {
      while (*begin != NULL)
	{
	  tmp = *begin;
	  *begin = tmp->next;
	  tmp->next = prev;
	  prev = tmp;
	}
      *begin = tmp;
    }
}
