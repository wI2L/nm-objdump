/*
** list_add_nosort__32.c for my_nm in /home/poussi_w//projects/nm-objdump/nm/i386
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb 22 22:43:22 2012 william poussier
** Last update Wed Feb 22 22:43:25 2012 william poussier
*/

#include <stdlib.h>
#include "my_nm.h"

void		list_add_nosort__32(t_list_32 *elem, t_list_32 **begin)
{
  t_list_32	*tmp;
  t_list_32	*clist;

  tmp = NULL;
  clist = *begin;

  while (clist != NULL)
    {
      tmp = clist;
      clist = clist->next;
    }
  elem->next = clist;

  if (tmp != NULL)
    tmp->next = elem;
  else
    *begin = elem;
}
