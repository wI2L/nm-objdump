/*
** iterate_symtype_over_scns__32.c for my_nm in /home/poussi_w//projects/nm-objdump/nm/i386
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb 22 22:42:36 2012 william poussier
** Last update Fri Mar  9 04:35:43 2012 william poussier
*/

#include <elf.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include "my_nm.h"

extern t_syms_type	symst_db[];

char			iterate_symtype_over_scns__32(char *str)
{
  int			i;

  i = -1;

  while (++i < 19)
    {
      if (strncmp(str, symst_db[i].scn, strlen(symst_db[i].scn)) == 0)
	return (symst_db[i].type);

    }
  return ('?');
}
