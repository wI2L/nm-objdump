/*
** list_add_numsort__32.c for my_nm in /home/poussi_w//projects/nm-objdump/nm/i386
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb 22 22:54:57 2012 william poussier
** Last update Fri Mar  9 07:33:02 2012 william poussier
*/

#include <elf.h>
#include <string.h>
#include <stdlib.h>
#include "my_nm.h"

void		list_numsort__32(t_list_32 **list)
{
  t_list_32	*tmp1;
  t_list_32	*tmp2;

  if (list != NULL && *list != NULL)
    {
      tmp1 = *list;
      while (tmp1->next != NULL)
	{
	  tmp2 = tmp1->next;
	  while (tmp2 != NULL)
	    {
	      if (tmp1->sym->st_value > tmp2->sym->st_value)
		my_node_swap__32(tmp1, tmp2);
	      if (tmp1->sym->st_value == tmp2->sym->st_value)
		{
		  if (strcmp(tmp1->str, tmp2->str) > 0)
		    my_node_swap__32(tmp1, tmp2);
		}
	      tmp2 = tmp2->next;
	    }
	  tmp1 = tmp1->next;
	}
    }
}
