/*
** dump_print__32.c for my_nm in /home/poussi_w//projects/nm-objdump/nm/i386
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb 22 22:41:19 2012 william poussier
** Last update Wed Feb 22 22:41:22 2012 william poussier
*/

#include <elf.h>
#include <stdlib.h>
#include "my_nm.h"

void		dump_print__32(void *buf, int nb_syms,
			       Elf32_Shdr *symscn, Elf32_Shdr *strscn)
{
  int		i;
  char		*strtab;
  t_list_32	*list;
  Elf32_Sym	*sym;

  i = 0;
  list = NULL;

  sym = buf + symscn->sh_offset;
  strtab = buf + strscn->sh_offset;

  if (sym != NULL)
    {
      while (i < nb_syms)
	{
	  if (sym[i].st_info != 4 && strtab[sym[i].st_name] != 0)
	    list_add__32(&sym[i], strtab + sym[i].st_name, &list);
	  i++;
	}
      list_print__32(buf, &list);
      list_free__32(&list);
    }
}
