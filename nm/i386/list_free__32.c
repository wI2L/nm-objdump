/*
** list_free__32.c for my_nm in /home/poussi_w//projects/nm-objdump/nm/i386
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb 22 22:43:51 2012 william poussier
** Last update Wed Feb 22 22:43:54 2012 william poussier
*/

#include <elf.h>
#include <stdlib.h>
#include "my_nm.h"

void		list_free__32(t_list_32 **begin)
{
  t_list_32	*tmp;
  t_list_32	*cur;

  cur = *begin;

  while (cur != NULL)
    {
      tmp = cur;
      cur = cur->next;
      free(tmp);
    }
}
