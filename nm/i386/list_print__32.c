/*
** list_print__32.c for my_nm in /home/poussi_w//projects/nm-objdump/nm/x86_64
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb 22 22:55:47 2012 william poussier
** Last update Sat Mar 10 02:20:36 2012 william poussier
*/

#include <elf.h>
#include <stdio.h>
#include <stdlib.h>
#include "my_nm.h"

void		list_print__32(void *buf, t_list_32 **begin)
{
  t_nm_options	*op;
  t_list_32	*tmp;

  op = nm_get_options();

  if (op->n && !op->p)
    {
      list_alphasort__32(begin);
      list_numsort__32(begin);
    }
  else if (!op->p)
    {
      list_numsort__32(begin);
      list_alphasort__32(begin);
    }
  if (op->r && !op->p)
    list_rev__32(begin);

  tmp = *begin;

  while (tmp != NULL)
    {
      print_symbol__32(buf, tmp, op);
      tmp = tmp->next;
    }
}
