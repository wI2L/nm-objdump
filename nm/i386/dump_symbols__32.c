/*
** dump_symbols__32.c for my_nm in /home/poussi_w//projects/nm-objdump/nm/i386
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb 22 22:41:48 2012 william poussier
** Last update Sat Mar 10 01:57:28 2012 william poussier
*/

#include <elf.h>
#include <stdio.h>
#include <stdlib.h>
#include "my_nm.h"

void		dump_symbols__32(void *buf, char *file)
{
  Elf32_Shdr	*symscn;
  Elf32_Shdr	*strscn;
  t_nm_options	*op;
  int		nb_syms;

  symscn = NULL;
  strscn = NULL;
  op = nm_get_options();

  nb_syms = 0;
  op->filename = file;

  if (dump_search__32(buf, &symscn, &strscn))
    {
      fprintf(stderr, NO_SYMBOLS, get_progname(NULL), file);
      exit(EXIT_FAILURE);
    }

  nb_syms = symscn->sh_size / sizeof(Elf32_Sym);

  dump_print__32(buf, nb_syms, symscn, strscn);
}
