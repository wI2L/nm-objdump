/*
** dump_search__32.c for my_nm in /home/poussi_w//projects/nm-objdump/nm/i386
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb 22 22:41:34 2012 william poussier
** Last update Sat Mar 10 01:58:32 2012 william poussier
*/

#include <elf.h>
#include <stdio.h>
#include <stdlib.h>
#include "my_nm.h"
#include "utils.h"

int		dump_search__32(void *buf, Elf32_Shdr **sym, Elf32_Shdr **str)
{
  t_nm_options	*op;

  op = nm_get_options();

  if (op->D)
    {
      *sym = elf_getscn_by_name__32(buf, ".dynsym");
      *str = elf_getscn_by_name__32(buf, ".dynstr");
    }
  else
    {
      *sym = elf_getscn_by_type__32(buf, SHT_SYMTAB);
      *str = elf_getscn_by_name__32(buf, ".strtab");
    }

  if (*sym == NULL || *str == NULL)
    return (1);

  return (0);
}
