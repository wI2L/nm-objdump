/*
** get_symbtype__32.c for my_nm in /home/poussi_w//projects/nm-objdump/nm/i386
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Wed Feb 22 22:42:23 2012 william poussier
** Last update Fri Mar  9 05:05:55 2012 william poussier
*/

#include <elf.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include "my_nm.h"

char		decode_section_type__32(Elf32_Shdr *scn)
{
  if ((scn->sh_flags & SHF_EXECINSTR) != 0)
    return ('t');

  if ((scn->sh_flags & SHF_ALLOC) != 0
      && scn->sh_type != SHT_NOBITS)
    {
      if ((scn->sh_flags & SHF_WRITE) == 0)
	return ('r');
      else
	return ('d');
    }
  if (scn->sh_type == SHT_NOBITS)
    return ('b');

  if ((scn->sh_type != SHT_NOBITS) && ((scn->sh_flags & SHF_WRITE) == 0))
    return ('n');

  return ('?');
}

char		decode_symclass__32(void *buf, Elf32_Sym *sym)
{
  Elf32_Ehdr	*elf;
  Elf32_Shdr	*scn;
  char		*str;
  char		c;

  elf = buf;
  scn = buf + elf->e_shoff + (elf->e_shentsize * sym->st_shndx);
  str = elf_getshstrtab__32(buf) + scn->sh_name;

  if (ELF32_ST_BIND(sym->st_info) == STB_GNU_UNIQUE)
    return ('u');

  if (sym->st_shndx)
    {
      c = iterate_symtype_over_scns__32(str);
      if (c == '?')
	c = decode_section_type__32(scn);
    }
  else
    return ('?');

  if (ELF32_ST_BIND(sym->st_info) == STB_GLOBAL)
    c = toupper(c);

  return (c);
}

char		get_symbtype__32(void *buf, Elf32_Sym *sym)
{
  if (sym->st_shndx == SHN_ABS)
    return ((ELF64_ST_BIND(sym->st_info) == STB_GLOBAL) ? 'A' : 'a');

  if (sym->st_shndx == SHN_COMMON)
    return ('C');

  if (sym->st_shndx == SHN_UNDEF)
    {
      if (ELF32_ST_BIND(sym->st_info) == STB_WEAK)
	return ((ELF32_ST_TYPE(sym->st_info) == STT_OBJECT) ? 'v' : 'w');
      else
	return ('U');
    }
  if (ELF32_ST_TYPE(sym->st_info) == STT_GNU_IFUNC)
    return ('i');

  if (ELF32_ST_BIND(sym->st_info) == STB_WEAK)
    return ((ELF32_ST_TYPE(sym->st_info) == STT_OBJECT) ? 'V' : 'W');

  return (decode_symclass__32(buf, sym));
}
