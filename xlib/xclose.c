/*
** xclose.c for nm-objdump in /home/poussi_w//projects/nm-objdump/xlib
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Dec 29 11:33:13 2011 william poussier
** Last update Tue Jan  3 00:05:47 2012 william poussier
*/

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "my_objdump.h"
#include "my_nm.h"
#include "xlib.h"

int		xclose(int fd)
{
  int		err;

  if (close(fd) == -1)
    {
      err = errno;
      fprintf(stderr, ERR_XCLOSE,
	      get_progname(NULL), strerror(err));

      return (EXIT_FAILURE);
    }
  return (0);
}
