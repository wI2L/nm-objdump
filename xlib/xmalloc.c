/*
** xmalloc.c for nm-objdump in /home/poussi_w//projects/nm-objdump/xlib
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Dec 29 11:31:48 2011 william poussier
** Last update Tue Jan  3 00:11:14 2012 william poussier
*/

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include "my_objdump.h"
#include "my_nm.h"
#include "xlib.h"

void		*xmalloc(size_t size)
{
  int		err;
  void		*none;

  if ((none = malloc(size)) == NULL)
    {
      err = errno;

      fprintf(stderr, ERR_XMALLOC,
	      get_progname(NULL), size, strerror(err));

      exit(EXIT_FAILURE);
    }
  return (none);
}
