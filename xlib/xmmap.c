/*
** xmmap.c for nm-objdump in /home/poussi_w//projects/nm-objdump/xlib
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Dec 29 11:33:48 2011 william poussier
** Last update Tue Jan  3 00:12:40 2012 william poussier
*/

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <sys/mman.h>
#include "my_objdump.h"
#include "my_nm.h"
#include "xlib.h"

void		*xmmap(void *addr, size_t length, int prot,
		       int flags, int fd, int offset)
{
  void		*buf;
  int		err;

  if ((buf = mmap(addr, length, prot, flags, fd, offset)) == MAP_FAILED)
    {
      err = errno;
      fprintf(stderr, ERR_XMMAP,
	      get_progname(NULL), strerror(err));

      exit(EXIT_FAILURE);
    }
  return (buf);
}
