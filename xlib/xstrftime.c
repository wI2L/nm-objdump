/*
** xstrftime.c for nm-objdump in /home/poussi_w//projects/nm-objdump/xlib
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Sun Jan 29 17:37:00 2012 william poussier
** Last update Sun Jan 29 17:42:57 2012 william poussier
*/

#include <time.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include "my_objdump.h"
#include "my_nm.h"
#include "xlib.h"

size_t		xstrftime(char *s, size_t max, const char *format,
			  const struct tm *tm)
{
  int           err;
  size_t        res;

  if ((res = strftime(s, max, format, tm)) == 0)
    {
      err = errno;
      fprintf(stderr,
	      "%s: cannot retrieve the local formatted date: %s\n",
	      get_progname(NULL), strerror(err));

      strcpy(s, ERR_DATE_MSG);
    }
  return (res);
}
