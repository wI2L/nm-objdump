/*
** xmunmap.c for nm-objdump in /home/poussi_w//projects/nm-objdump/xlib
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Dec 29 11:34:04 2011 william poussier
** Last update Tue Jan  3 00:13:24 2012 william poussier
*/

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <sys/mman.h>
#include "my_objdump.h"
#include "my_nm.h"
#include "xlib.h"

int		xmunmap(void *addr, size_t length)
{
  int		ret;
  int		err;

  if ((ret = munmap(addr, length)) == -1)
    {
      err = errno;
      fprintf(stderr, ERR_XMUNMAP,
	      get_progname(NULL), strerror(err));

      exit(EXIT_FAILURE);
    }
  return (ret);
}
