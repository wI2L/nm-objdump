/*
** xopen.c for nm-objdump in /home/poussi_w//projects/nm-objdump/xlib
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Dec 29 11:31:23 2011 william poussier
** Last update Tue Jan  3 00:08:28 2012 william poussier
*/

#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include "my_objdump.h"
#include "my_nm.h"
#include "xlib.h"

int		xopen(const char* pathname, int flags, char *file)
{
  int		err;
  int		fd;

  if ((fd = open(pathname, flags)) == -1)
    {
      err = errno;

      if (err == ENOENT)
	fprintf(stderr, ERR_XOPEN_STD,
		get_progname(NULL), file);
      else
	fprintf(stderr, ERR_XOPEN_ADVC,
		get_progname(NULL), file, strerror(err));

      exit(EXIT_FAILURE);
    }
  return (fd);
}
