/*
** xfstat.c for nm-objdump in /home/poussi_w//projects/nm-objdump/xlib
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Dec 29 11:33:32 2011 william poussier
** Last update Tue Jan  3 00:10:30 2012 william poussier
*/

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "my_objdump.h"
#include "my_nm.h"
#include "xlib.h"

int		xfstat(int fd, struct stat *buf, char *file)
{
  int		ret;
  int		err;

  if ((ret = fstat(fd, buf)) == -1)
    {
      err = errno;
      fprintf(stderr, ERR_XFSTAT_STD,
	      get_progname(NULL), strerror(err));

      exit(EXIT_FAILURE);
    }

  if (!S_ISREG(buf->st_mode))
    {
      fprintf(stderr, ERR_XFSTAT_REG,
	      get_progname(NULL), file);

      exit(EXIT_FAILURE);
    }
  return (ret);
}
